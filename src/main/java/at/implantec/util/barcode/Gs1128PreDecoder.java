package at.implantec.util.barcode;

import org.apache.commons.lang3.StringUtils;

final class Gs1128PreDecoder {

    private final String barcode;

    Gs1128PreDecoder(String barcode) {
        this.barcode = barcode;
    }

    public static void main(String[] args) {

        String barcode = "]C1010539123456789215B546317051231";
        String barcodeOnlyLot = "]C110APO2317051124";
        String barcodeMultiScan = "]C110A160337172309000109120018480647";
        String barcodeWithOutExpiry = "]C1010425081690092610FD798";
        Gs1128PreDecoder preDecoder = new Gs1128PreDecoder(barcodeWithOutExpiry);
        String barcodeValue = StringUtils.remove(barcodeWithOutExpiry, Gs1128Utils.PREFIX);
        // System.out.println(preDecoder.prepared());
        System.out.println(barcodeValue);
        // System.out.println(barcodeValue.indexOf("10"));
        // System.out.println(barcodeValue.lastIndexOf("10"));
        System.out.println(barcodeValue.length()); // - barcodeValue.lastIndexOf("17"));
    }

    public String prepared() {
        String barcodeValue = StringUtils.remove(barcode, Gs1128Utils.PREFIX);
        String barcodePrepared = barcodeValue;
        if (barcodeValue.indexOf("10") == 0 || barcodeValue.lastIndexOf("10") == 16) {
            if (barcodeValue.length() - barcodeValue.lastIndexOf("17") == 8) {
                barcodePrepared = barcodeValue.substring(barcodeValue.length() - 8)
                        + barcodeValue.substring(0, barcodeValue.length() - 8);
            } else if (barcodeValue.length() - barcodeValue.lastIndexOf("17") == 24
                    && barcodeValue.length() > (barcodeValue.length() - barcodeValue.lastIndexOf("17"))) {
                barcodePrepared = barcodeValue.substring(barcodeValue.length() - 24)
                        + barcodeValue.substring(0, barcodeValue.length() - 24);
            }
        }
        return barcodePrepared;
    }
}
