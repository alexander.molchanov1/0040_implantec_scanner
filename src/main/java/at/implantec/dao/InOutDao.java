package at.implantec.dao;

import at.implantec.model.DAOInOut;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InOutDao extends CrudRepository<DAOInOut, Integer> {

    List findByIssotrx(String issotrx);
}
