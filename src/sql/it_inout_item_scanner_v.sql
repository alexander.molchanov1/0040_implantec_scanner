DROP VIEW it_inout_item_scanner_v;
CREATE
OR REPLACE VIEW it_inout_item_scanner_v AS
WITH
inout AS
(
SELECT 
		it_inout_id, c_bpartner_id, issotrx, documentno 
	FROM 
		it_inout 
	WHERE 
        docstatus = 'DR'
    AND
		it_scanstatus IN ('PR', 'SC')
),
product AS 
(
	SELECT 
		m_product_id, value, name, upc, it_locatorname 
	FROM 
		m_product 
	WHERE 
		iststocked='Y'
	AND 
        isactive='Y'
    AND
        ad_client_id = 1000000
),
bpartner AS
(
	SELECT 
		c_bpartner_id, name
	FROM 
		c_bpartner 
	WHERE 
		isactive='Y'
	AND 
        ad_client_id = 1000000
)
SELECT a.it_inout_id,
       a.documentno,
       a.bpartner_name,
       a.it_inoutline_id,
       a.line,
       a.quantity,
       a.it_warehouse_id,
       a.m_product_id,
       a.value,
       a.product_name,
       a.upc,
       a.it_locatorname,
       a.issotrx,
       a.doc_type,
       a.pos_total,
       a.product_total,
       a.qty_scanned,
       a.scanned_total,
       (a.quantity - a.qty_scanned)        AS open_inposition,
       (a.product_total - a.scanned_total) AS open_total
FROM (SELECT io.it_inout_id,
             io.documentno,
             cbp.name                                                                                AS bpartner_name,
             iol.it_inoutline_id,
             iol.line,
             iol.qtyentered                                                                          AS quantity,
             iol.it_warehouse_id,
             pr.m_product_id,
             pr.value,
             pr.name                                                                                 AS product_name,
             pr.upc,
             pr.it_locatorname,
             io.issotrx,
             CASE WHEN io.issotrx = 'Y' THEN 'MF' ELSE 'MT' END                                      AS doc_type,
             CASE WHEN COALESCE(MAX(iol2lot.it_lot_id), 0) > 0 THEN SUM(iol2lot.quantity) ELSE 0 END AS qty_scanned,
             (SELECT COUNT(DISTINCT iol1.it_inoutline_id)
              FROM it_inoutline AS iol1
              WHERE iol1.it_inout_id = iol.it_inout_id)                                              AS pos_total,
             (SELECT SUM(iol1.qtyentered)
              FROM it_inoutline AS iol1
              WHERE iol1.it_inout_id = iol.it_inout_id
              GROUP BY iol.it_inout_id)                                                              AS product_total,
             COALESCE((SELECT SUM(iol2lot1.quantity)
                       FROM it_inoutline2lot AS iol2lot1
                       WHERE iol2lot1.it_inoutline_id IN (SELECT iol1.it_inoutline_id
                                                          FROM it_inoutline iol1
                                                          WHERE iol1.it_inout_id = iol.it_inout_id)
                         AND iol2lot1.it_lot_id IS NOT NULL
                       GROUP BY iol.it_inout_id), 0)                                                 AS scanned_total
      FROM inout io
               JOIN bpartner cbp USING (c_bpartner_id)
               JOIN it_inoutline iol USING (it_inout_id)
               JOIN it_inoutline2lot iol2lot USING (it_inoutline_id)
               JOIN product pr ON pr.m_product_id = iol.m_product_id
      GROUP BY io.it_inout_id,
               io.documentno,
               cbp.name,
               iol.it_inoutline_id,
               iol.line,
               iol.qtyentered,
               iol.it_warehouse_id,
               pr.m_product_id,
               pr.value,
               pr.name,
               pr.upc,
               pr.it_locatorname,
               io.issotrx
      ORDER BY pr.it_locatorname) a;

ALTER TABLE adempiere.it_inout_item_scanner_v
    OWNER TO adempiere;