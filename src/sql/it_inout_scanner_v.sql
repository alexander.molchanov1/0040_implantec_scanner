--DROP VIEW it_inout_scanner_v;

CREATE
OR REPLACE VIEW it_inout_scanner_v AS
SELECT DISTINCT io.it_inout_id,
                io.documentno,
                io.issotrx,
                io.it_scanstatus,
                bp.name,
                count(iol.it_inoutline_id) over (partition by iol.it_inout_id) AS positions
FROM it_inout io
         JOIN c_bpartner bp ON io.c_bpartner_id = bp.c_bpartner_id
         JOIN it_inoutline iol ON io.it_inout_id = iol.it_inout_id

WHERE io.docstatus IN ('DR')
  AND io.it_scanstatus IN ('SC', 'PR');

ALTER TABLE adempiere.it_inout_scanner_v
    OWNER TO adempiere;