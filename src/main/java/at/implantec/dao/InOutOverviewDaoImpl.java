package at.implantec.dao;

import at.implantec.mapper.InOutOverviewRowMapper;
import at.implantec.model.DAOInOutOverview;
import java.util.List;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class InOutOverviewDaoImpl implements InOutOverviewDao {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private List<DAOInOutOverview> inoutover;

    public InOutOverviewDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<DAOInOutOverview> findAllById(Long id) {
        final String sql = "SELECT * FROM it_inout_overview_scanner_v WHERE it_inout_id=:it_inout_id";
        SqlParameterSource param = new MapSqlParameterSource().addValue("it_inout_id", id);
        return jdbcTemplate.query(sql, param, new InOutOverviewRowMapper());
    }
}
