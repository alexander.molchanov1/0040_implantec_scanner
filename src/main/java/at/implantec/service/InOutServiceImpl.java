package at.implantec.service;

import at.implantec.dao.InOutDao;
import at.implantec.model.DAOInOut;
import jakarta.annotation.Resource;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class InOutServiceImpl implements InOutService {
    @Resource
    InOutDao inoutDao;

    @Override
    public List<DAOInOut> findByIssotrx(String issotrx) {
        return inoutDao.findByIssotrx(issotrx);
    }
}
