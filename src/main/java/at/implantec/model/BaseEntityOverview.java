package at.implantec.model;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;

/**
 * Base class.
 *
 * @author Alexander Molchanov
 * @version 1.3
 */
@MappedSuperclass
public class BaseEntityOverview {

    @Column(name = "countactuallyscanned")
    private Integer countactuallyscanned;

    @Column(name = "countshouldbescanned")
    private Integer countshouldbescanned;

    @Column(name = "productname")
    private String productname;

    @Column(name = "productnumber")
    private String productnumber;

    @Column(name = "counttoscan")
    private Integer counttoscan;

    public Integer getCountactuallyscanned() {
        return countactuallyscanned;
    }

    public void setCountactuallyscanned(Integer countactuallyscanned) {
        this.countactuallyscanned = countactuallyscanned;
    }

    public Integer getCountshouldbescanned() {
        return countshouldbescanned;
    }

    public void setCountshouldbescanned(Integer countshouldbescanned) {
        this.countshouldbescanned = countshouldbescanned;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getProductnumber() {
        return productnumber;
    }

    public void setProductnumber(String productnumber) {
        this.productnumber = productnumber;
    }

    public Integer getCounttoscan() {
        return counttoscan;
    }

    public void setCounttoscan(Integer counttoscan) {
        this.counttoscan = counttoscan;
    }
}
