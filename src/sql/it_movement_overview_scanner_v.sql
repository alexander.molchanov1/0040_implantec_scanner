--DROP VIEW it_movement_overview_scanner_v;
/*
CREATE OR REPLACE VIEW it_movement_overview_scanner_v AS
SELECT mv.it_movement_id,
       pr.value AS productNumber,
       pr.name AS productName, 
       mvl.quantity AS countShouldBeScanned, 
       SUM(CASE WHEN mvl2l.it_lot_id IS NULL THEN 0 ELSE mvl2l.quantity END) AS countActuallyScanned
FROM it_movement mv 
LEFT JOIN it_movementline mvl  ON mv.it_movement_id = mvl.it_movement_id
LEFT JOIN it_movementline2lot mvl2l ON mvl.it_movementline_id = mvl2l.it_movementline_id
JOIN m_product pr on pr.m_product_id = mvl.m_product_id
WHERE  mv.docstatus IN ('DR') AND mv.it_scanstatus IN ('SC', 'PR')
GROUP BY mv.it_movement_id,
       pr.value,
       pr.name, 
       mvl.quantity;

ALTER TABLE adempiere.it_movement_overview_scanner_v
  OWNER TO adempiere;
*/
--NEW VERSION TO TEST
DROP VIEW it_movement_overview_scanner_v;

CREATE
OR REPLACE VIEW it_movement_overview_scanner_v AS
SELECT a.it_movement_id,
       a.it_movementline_id,
       a.productNumber,
       a.productName,
       a.countShouldBeScanned,
       a.countActuallyScanned,
       a.countShouldBeScanned - a.countActuallyScanned AS counttoscan
FROM (SELECT mv.it_movement_id,
             mvl.it_movementline_id,
             pr.value                                                              AS productNumber,
             pr.name                                                               AS productName,
             mvl.quantity                                                          AS countShouldBeScanned,
             SUM(CASE WHEN mvl2l.it_lot_id IS NULL THEN 0 ELSE mvl2l.quantity END) AS countActuallyScanned
      FROM it_movement mv
               LEFT JOIN it_movementline mvl ON mv.it_movement_id = mvl.it_movement_id
               LEFT JOIN it_movementline2lot mvl2l ON mvl.it_movementline_id = mvl2l.it_movementline_id
               JOIN m_product pr on pr.m_product_id = mvl.m_product_id
      WHERE mv.docstatus IN ('DR')
        AND mv.it_scanstatus IN ('SC', 'PR')
      GROUP BY mv.it_movement_id,
               mvl.it_movementline_id,
               pr.value,
               pr.name,
               mvl.quantity) a;

ALTER TABLE adempiere.it_movement_overview_scanner_v
    OWNER TO adempiere;