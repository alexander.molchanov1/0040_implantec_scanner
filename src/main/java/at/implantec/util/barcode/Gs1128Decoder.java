package at.implantec.util.barcode;

import static at.implantec.util.barcode.AiFactory.create;

import java.util.*;
import java.util.function.Function;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

/**
 * @author Alexander Molchanov
 * <p>
 * Version 1.1
 */
public final class Gs1128Decoder {

    private final Collection<AI> ais;

    Gs1128Decoder(Collection<AI> ais) {
        this.ais = ais;
    }

    public static void main(String[] args) throws Exception {
        Gs1128Engine engine = new Gs1128Engine();
        AI NSN = create("+M248", "EAN39 number", "n4+n7");
        engine.registerAi(NSN);
        String barcodePrepared = "]C117051231010539123456789210B5463";
        String barcodeLotFirst = "]C1010539123456789210ZGHB5463AB2417051231";
        String barcodeLotAndExpiry = "]C110APO2317051124";
        String barcodeOnlyLot = "]C110APOZRT23S";
        String barcodeExpityFirst = "]C101053912345678921705123110B5463";
        String barcodeWithNsN = "]C101053912345678921505123110APO1536Z";
        String barcodeMultiScan = "]C110A160337172309000109120018480647";
        String codeEAN39 = "]C1+M248285330SND1Y";
        String barcodeWithOutExpiry = "]C1010425081690092610FD798";

        Gs1128Decoder decoder = engine.decoder();
        System.out.println(new Gs1128PreDecoder(barcodeWithOutExpiry).prepared());
        // Map<AI, String> result = decoder.decode(barcodePrepared);
        Map<AI, String> result = decoder.decode(barcodeWithOutExpiry);
        System.out.println("UPC: " + result.get(AIs.GTIN));
        System.out.println("Lot: " + result.get(AIs.BATCH_LOT));
        System.out.println("Expiry: " + result.get(AIs.EXPIRY));
        // System.out.println("Best before: " + result.get(AIs.BEST_BEFORE));
        // System.out.println("NsN: " + result.get(NSN));
    }

    public Map<AI, String> decode(final String barcode) {
        validateBarcode(barcode);
        String barcodeValue = new Gs1128PreDecoder(barcode).prepared();
        return extract(barcodeValue);
    }

    private Map<AI, String> extract(String barcode) {
        final Iterator<Character> iterator = Gs1128Utils.iterator(barcode);
        final Map<AI, String> result = new WeakHashMap<>(4);
        StringBuilder builder = new StringBuilder();
        while (iterator.hasNext()) {
            builder.append(iterator.next());
            Optional<Map<AI, String>> mapAi = mapAi(iterator, builder.toString());
            if (mapAi.isPresent()) {
                result.putAll(mapAi.get());
                builder = new StringBuilder();
            }
        }
        return result;
    }

    private Optional<Map<AI, String>> mapAi(Iterator<Character> iterator, String builder) {
        return ais.stream().filter(v -> v.getCode().equals(builder)).findFirst().map(aiValue(iterator));
    }

    private Function<AI, Map<AI, String>> aiValue(Iterator<Character> iterator) {
        return ai -> {
            String value = Gs1128Utils.value(ai, iterator);
            return Map.of(ai, value);
        };
    }

    private void validateBarcode(String barcode) {
        Validate.notBlank(barcode, "Barcode cannot be blank");
        Validate.isTrue(
                StringUtils.startsWith(barcode, Gs1128Utils.PREFIX), "Barcode must start with prefix Gs1-128 \"]C1\"");
    }
}
