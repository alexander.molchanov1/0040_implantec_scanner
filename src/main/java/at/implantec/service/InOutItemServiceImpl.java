package at.implantec.service;

import at.implantec.dao.InOutItemDao;
import at.implantec.model.DAOInOutItem;
import jakarta.annotation.Resource;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class InOutItemServiceImpl implements InOutItemService {
    @Resource
    InOutItemDao inoutItemDao;

    @Override
    public List<DAOInOutItem> findAllPosByIoid(Long io_id) {
        return (List<DAOInOutItem>) inoutItemDao.findAllPosByIoid(io_id);
    }
}
