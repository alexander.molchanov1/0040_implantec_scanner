package at.implantec.mapper;

import at.implantec.model.DAOMovementOverview;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class MovementOverviewRowMapper implements RowMapper<DAOMovementOverview> {

    @Override
    public DAOMovementOverview mapRow(ResultSet rs, int rowNum) throws SQLException {

        DAOMovementOverview movover = new DAOMovementOverview();
        movover.setId(rs.getLong("it_movement_id"));
        movover.setMovementline(rs.getLong("it_movementline_id"));
        movover.setCountactuallyscanned(rs.getInt("countactuallyscanned"));
        movover.setCountshouldbescanned(rs.getInt("countshouldbescanned"));
        movover.setProductnumber(rs.getString("productnumber"));
        movover.setProductname(rs.getString("productname"));
        movover.setCounttoscan(rs.getInt("counttoscan"));

        return movover;
    }
}
