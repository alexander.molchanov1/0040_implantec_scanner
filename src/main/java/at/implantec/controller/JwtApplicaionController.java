package at.implantec.controller;

import at.implantec.model.*;
import at.implantec.security.JwtTokenUtil;
import at.implantec.service.*;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@Tag(name = "Jwt Application Controller", description = "Main controller with bearer token authentication")
public class JwtApplicaionController {

    @Resource
    MovementService movementService;

    @Resource
    MovementItemService movementItemService;

    @Resource
    Lot2MovementItemService lot2MovementItemService;

    @Resource
    MovementOverviewService movementOverviewService;

    @Resource
    MovementItem2LotService movementItem2LotService;

    @Resource
    InOutService inoutService;

    @Resource
    InOutItemService inoutItemService;

    @Resource
    InOutOverviewService inoutOverviewService;

    @Resource
    InOutItem2LotService inoutItem2LotService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequestLogin authenticationRequest)
            throws Exception {

        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtResponseLogin(true, authenticationRequest.getUsername(), token));
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    @RequestMapping(value = "/movements", params = "issotrx", method = RequestMethod.GET)
    @SecurityRequirement(name = "JWT")
    public List<DAOMovement> getMovements(@RequestParam(required = false, name = "issotrx") String issotrx) {
        return movementService.findByIssotrx(issotrx);
    }

    @RequestMapping(value = "/movement_position", params = "io_id", method = RequestMethod.GET)
    @SecurityRequirement(name = "JWT")
    public Map<String, List<DAOMovementItem>> getMovementItems(
            @RequestParam(required = false, name = "io_id") Long io_id) {
        Map<String, List<DAOMovementItem>> response = new HashMap<String, List<DAOMovementItem>>();
        response.put("position", movementItemService.findAllPosByIoid(io_id));
        return response;
    }

    @RequestMapping(value = "/lot_position", params = "mvline_id", method = RequestMethod.GET)
    @SecurityRequirement(name = "JWT")
    public Map<String, List<DAOLot2MovementItem>> getLot2MovementItems(
            @RequestParam(required = false, name = "mvline_id") Long mvline_id) {
        Map<String, List<DAOLot2MovementItem>> response = new HashMap<String, List<DAOLot2MovementItem>>();
        response.put("lots", lot2MovementItemService.findAllPosByMovementline(mvline_id));
        return response;
    }

    @RequestMapping(value = "/movement_overview", params = "id", method = RequestMethod.GET)
    @SecurityRequirement(name = "JWT")
    public List<DAOMovementOverview> getMovementOverview(@RequestParam(required = false, name = "id") Long id) {
        return movementOverviewService.findAllById(id);
    }

    @RequestMapping(value = "/movement_scan-code", method = RequestMethod.POST)
    @SecurityRequirement(name = "JWT")
    public ResponseEntity<?> updateMovementItem2Lot(@RequestBody DAOMovementItem2Lot mvl2lot) {
        movementItem2LotService.updateMovementItem2Lot(mvl2lot);
        return ResponseEntity.status(movementItem2LotService.getHttpStatus())
                .body(new ResponseScanCode(movementItem2LotService.getMessage()));
    }

    @RequestMapping(value = "/inouts", params = "issotrx", method = RequestMethod.GET)
    @SecurityRequirement(name = "JWT")
    public List<DAOInOut> getInout(@RequestParam(required = false, name = "issotrx") String issotrx) {
        return inoutService.findByIssotrx(issotrx);
    }

    @RequestMapping(value = "/inout_position", params = "io_id", method = RequestMethod.GET)
    @SecurityRequirement(name = "JWT")
    public Map<String, List<DAOInOutItem>> getInoutItems(@RequestParam(required = false, name = "io_id") Long io_id) {
        Map<String, List<DAOInOutItem>> response = new HashMap<String, List<DAOInOutItem>>();
        response.put("position", inoutItemService.findAllPosByIoid(io_id));
        return response;
    }

    @RequestMapping(value = "/inout_overview", params = "id", method = RequestMethod.GET)
    @SecurityRequirement(name = "JWT")
    public List<DAOInOutOverview> getInOutOverview(@RequestParam(required = false, name = "id") Long id) {
        return inoutOverviewService.findAllById(id);
    }

    @RequestMapping(value = "/inout_scan-code", method = RequestMethod.POST)
    @SecurityRequirement(name = "JWT")
    public ResponseEntity<?> updateInOutItem2Lot(@RequestBody DAOInOutItem2Lot iol2lot) {
        inoutItem2LotService.updateInOutItem2Lot(iol2lot);
        return ResponseEntity.status(inoutItem2LotService.getHttpStatus())
                .body(new ResponseScanCode(inoutItem2LotService.getMessage()));
    }
}
