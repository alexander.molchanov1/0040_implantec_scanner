--DROP VIEW it_movement_scanner_v;

CREATE
OR REPLACE VIEW it_movement_scanner_v AS
SELECT DISTINCT mv.it_movement_id,
                mv.documentno,
                mv.issotrx,
                mv.it_scanstatus,
                bp.name,
                count(mvl.it_movementline_id) over (partition by mvl.it_movement_id) AS positions
FROM it_movement mv
         JOIN c_bpartner bp ON mv.c_bpartner_id = bp.c_bpartner_id
         JOIN it_movementline mvl ON mv.it_movement_id = mvl.it_movement_id

WHERE mv.docstatus IN ('DR')
  AND mv.it_scanstatus IN ('SC', 'PR');

ALTER TABLE adempiere.it_movement_scanner_v
    OWNER TO adempiere;