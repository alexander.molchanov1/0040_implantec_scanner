package at.implantec.service;

import at.implantec.dao.MovementItem2LotDao;
import at.implantec.model.DAOMovementItem2Lot;
import java.util.List;
import org.springframework.http.HttpStatus;

public interface MovementItem2LotService {

    MovementItem2LotDao findMovementItem2LotById(Long id);

    void insertMovementItem2Lot(DAOMovementItem2Lot mvl2lot);

    void updateMovementItem2Lot(DAOMovementItem2Lot mvl2lot);

    void deleteById(Long id);

    List<DAOMovementItem2Lot> getAllById(Long id);

    DAOMovementItem2Lot getByLineId(Long id);

    String getMessage();

    void setMessage(String message);

    HttpStatus getHttpStatus();

    void setHttpStatus(HttpStatus httpstatus);
}
