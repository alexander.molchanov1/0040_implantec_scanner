@echo off
setlocal EnableDelayedExpansion

call :setESC

:: Colors using ANSI escape codes (works in Windows 10+)
set "RED=%ESC%[91m"
set "GREEN=%ESC%[92m"
set "YELLOW=%ESC%[93m"
set "BLUE=%ESC%[94m"
set "NC=%ESC%[0m"

goto main

:setESC
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do rem"') do (
  set ESC=%%b
  exit /B 0
)
exit /B 0

:: Function for status messages
:print_status
echo %BLUE%[INFO]%NC% %~1
exit /b

:print_success
echo %GREEN%[SUCCESS]%NC% %~1
exit /b

:print_error
echo %RED%[ERROR]%NC% %~1
exit /b

:print_warning
echo %YELLOW%[WARNING]%NC% %~1
exit /b

:main

cls
echo %BLUE%
echo  ____           _ _     _    ____                                
echo ^|  _ \         ^| ^| ^|   / \  / ___^|
echo ^| ^|_) ^|_   _   ^| ^| ^|  / _ \ \___ \
echo ^|  _ ^<^| ^| ^| ^|  ^| ^| ^| / ___ \ ___) ^|
echo ^|_^| \_\ ^|_^|_^|  ^|_^|_^|/_/   \_\____/
echo.
echo %NC%

call :print_status "Building Docker images..."

docker-compose build
if %ERRORLEVEL% NEQ 0 (
    call :print_error "Failed to build Docker images"
    pause
    exit /b %ERRORLEVEL%
)
call :print_success "Docker images built successfully"

call :print_status "Starting services..."
docker-compose up -d
if %ERRORLEVEL% NEQ 0 (
    call :print_error "Failed to start services"
    pause
    exit /b %ERRORLEVEL%
)

call :print_success "Services started successfully!"
call :print_success "Application is running at http://localhost:3002"
pause