package at.implantec.service;

import at.implantec.model.DAOInOut;
import java.util.List;

public interface InOutService {

    List<DAOInOut> findByIssotrx(String param);
}
