--DROP VIEW it_product_upc_scanner_v;

CREATE
OR REPLACE VIEW it_product_upc_scanner_v AS
WITH movement AS (
SELECT mv.issotrx,
       mvl.it_movementline_id,
       0 AS it_inoutline_id,
       mvl.m_product_id,
       pr.upc
FROM it_movement mv 
LEFT JOIN it_movementline mvl  ON mv.it_movement_id = mvl.it_movement_id
JOIN m_product pr on pr.m_product_id = mvl.m_product_id
WHERE  mv.docstatus IN ('DR') AND mv.it_scanstatus IN ('SC', 'PR')
),
inout AS (
SELECT io.issotrx,
       0 AS it_movementline_id,
       iol.it_inoutline_id,
       iol.m_product_id,
       pr.upc
FROM it_inout io 
LEFT JOIN it_inoutline iol  ON io.it_inout_id = iol.it_inout_id
JOIN m_product pr on pr.m_product_id = iol.m_product_id
WHERE  io.docstatus IN ('DR') AND io.it_scanstatus IN ('SC', 'PR')
)
SELECT *
FROM movement
UNION ALL
SELECT *
FROM inout
;

ALTER TABLE adempiere.it_product_upc_scanner_v
    OWNER TO adempiere;