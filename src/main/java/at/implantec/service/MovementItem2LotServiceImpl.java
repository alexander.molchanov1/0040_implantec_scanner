package at.implantec.service;

import at.implantec.dao.MovementItem2LotDao;
import at.implantec.model.DAOMovementItem2Lot;
import jakarta.annotation.Resource;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class MovementItem2LotServiceImpl implements MovementItem2LotService {

    @Resource
    MovementItem2LotDao movementItem2LotDao;

    @Override
    public MovementItem2LotDao findMovementItem2LotById(Long id) {
        movementItem2LotDao.findMovementItem2LotById(id);
        return null;
    }

    @Override
    public void insertMovementItem2Lot(DAOMovementItem2Lot mvl2lot) {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateMovementItem2Lot(DAOMovementItem2Lot mvl2lot) {
        movementItem2LotDao.updateMovementItem2Lot(mvl2lot);
    }

    @Override
    public void deleteById(Long id) {
        // TODO Auto-generated method stub

    }

    @Override
    public List<DAOMovementItem2Lot> getAllById(Long id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public DAOMovementItem2Lot getByLineId(Long id) {
        // TODO Auto-generated method stub
        return new DAOMovementItem2Lot();
    }

    @Override
    public String getMessage() {
        return movementItem2LotDao.getMessage();
    }

    @Override
    public void setMessage(String message) {
        movementItem2LotDao.setMessage(message);
    }

    @Override
    public HttpStatus getHttpStatus() {
        return movementItem2LotDao.getHttpStatus();
    }

    @Override
    public void setHttpStatus(HttpStatus httpstatus) {
        movementItem2LotDao.setHttpStatus(httpstatus);
    }
}
