package at.implantec.model;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;

/**
 * Base class.
 *
 * @author Alexander Molchanov
 * @version 1.3
 */
@MappedSuperclass
public class BaseEntityHeader {

    @Column(name = "documentno")
    private String io_no;

    @Column(name = "issotrx")
    private String issotrx;

    @Column(name = "it_scanstatus")
    private String status_s;

    @Column(name = "name")
    private String bp_name;

    @Column(name = "positions")
    private String pos;

    public String getIo_no() {
        return io_no;
    }

    public String getIssotrx() {
        return issotrx;
    }

    public String getStatus_s() {
        return status_s.equals("PR") ? "wird gescannt" : "zu scannen";
    }

    public String getBp_name() {
        return bp_name;
    }

    public String getCount_pos() {
        return pos;
    }
}
