package at.implantec.mapper;

import at.implantec.model.DAOInOutOverview;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class InOutOverviewRowMapper implements RowMapper<DAOInOutOverview> {

    @Override
    public DAOInOutOverview mapRow(ResultSet rs, int rowNum) throws SQLException {

        DAOInOutOverview inoutover = new DAOInOutOverview();
        inoutover.setId(rs.getLong("it_inout_id"));
        inoutover.setInoutline(rs.getLong("it_inoutline_id"));
        inoutover.setCountactuallyscanned(rs.getInt("countactuallyscanned"));
        inoutover.setCountshouldbescanned(rs.getInt("countshouldbescanned"));
        inoutover.setProductnumber(rs.getString("productnumber"));
        inoutover.setProductname(rs.getString("productname"));
        inoutover.setCounttoscan(rs.getInt("counttoscan"));

        return inoutover;
    }
}
