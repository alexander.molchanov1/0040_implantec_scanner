package at.implantec.model;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import java.sql.Timestamp;

/**
 * Base class.
 *
 * @author Alexander Molchanov
 * @version 1.0
 */
@MappedSuperclass
public class BaseEntityLot2Position {

    @Column(name = "ad_client_id")
    private Integer ad_client_id;

    @Column(name = "ad_org_id")
    private Integer ad_org_id;

    @Column(name = "created")
    private Timestamp created;

    @Column(name = "createdby")
    private Integer createdby;

    @Column(name = "expected_lot_id")
    private Integer expected_lot_id;

    @Column(name = "isactive")
    private String isactive;

    @Column(name = "issotrx")
    private String issotrx;

    @Column(name = "it_locator_id")
    private Integer it_locator_id;

    @Column(name = "it_lot_id")
    private Integer it_lot_id;

    @Column(name = "m_product_id")
    private Integer m_product_id;

    @Column(name = "qtyexpected")
    private Integer qtyexpected;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "t_locator_id")
    private Integer t_locator_id;

    @Column(name = "updated")
    private Timestamp updated;

    @Column(name = "updatedby")
    private Integer updatedby;

    public Integer getAd_client_id() {
        return ad_client_id;
    }

    public void setAd_client_id(Integer ad_client_id) {
        this.ad_client_id = ad_client_id;
    }

    public Integer getAd_org_id() {
        return ad_org_id;
    }

    public void setAd_org_id(Integer ad_org_id) {
        this.ad_org_id = ad_org_id;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public Integer getExpected_lot_id() {
        return expected_lot_id;
    }

    public void setExpected_lot_id(Integer expected_lot_id) {
        this.expected_lot_id = expected_lot_id;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public String getIssotrx() {
        return issotrx;
    }

    public void setIssotrx(String issotrx) {
        this.issotrx = issotrx;
    }

    public Integer getIt_locator_id() {
        return it_locator_id;
    }

    public void setIt_locator_id(Integer it_locator_id) {
        this.it_locator_id = it_locator_id;
    }

    public Integer getIt_lot_id() {
        return it_lot_id;
    }

    public void setIt_lot_id(Integer it_lot_id) {
        this.it_lot_id = it_lot_id;
    }

    public Integer getM_product_id() {
        return m_product_id;
    }

    public void setM_product_id(Integer m_product_id) {
        this.m_product_id = m_product_id;
    }

    public Integer getQtyexpected() {
        return qtyexpected;
    }

    public void setQtyexpected(Integer qtyexpected) {
        this.qtyexpected = qtyexpected;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getT_locator_id() {
        return t_locator_id;
    }

    public void setT_locator_id(Integer t_locator_id) {
        this.t_locator_id = t_locator_id;
    }

    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    public Integer getUpdatedby() {
        return updatedby;
    }

    public void setUpdatedby(Integer updatedby) {
        this.updatedby = updatedby;
    }
}
