package at.implantec.dao;

import at.implantec.model.DAOLot2InOutItem;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Lot2InOutItemDao extends CrudRepository<DAOLot2InOutItem, Integer> {

    List findAllPosByInoutline(Long inoutline);
}
