package at.implantec.mapper;

import at.implantec.model.DAOProductUpc;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ProductUpcRowMapper implements RowMapper<DAOProductUpc> {

    @Override
    public DAOProductUpc mapRow(ResultSet rs, int rowNum) throws SQLException {

        DAOProductUpc prupc = new DAOProductUpc();
        prupc.setId(rs.getLong("line_id"));
        prupc.setDoctype(rs.getString("doctype"));
        prupc.setIssotrx(rs.getString("issotrx"));
        prupc.setProduct(rs.getLong("m_product_id"));
        prupc.setUpc(rs.getString("upc"));

        return prupc;
    }
}
