package at.implantec.model;

import java.io.Serializable;

public class JwtResponseLogin implements Serializable {

    private static final long serialVersionUID = -8091879091924046844L;
    private final Boolean auth;
    private final String jwttoken;
    private final String user;

    public JwtResponseLogin(boolean auth, String user, String jwttoken) {
        this.auth = auth;
        this.jwttoken = jwttoken;
        this.user = user;
    }

    public Boolean isAuth() {
        return this.auth;
    }

    public String getToken() {
        return this.jwttoken;
    }

    public String getUser() {
        return this.user;
    }
}
