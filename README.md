# ImplanTec Scanner Application 🚀

A Spring Boot application for the ImplanTec scanner system.

## 📋 Prerequisites

- 🐳 Docker Desktop ([Download here](https://www.docker.com/products/docker-desktop/))
- This will install both Docker and Docker Compose
- ☕ JDK 17 (for local development)
- 💾 At least 8GB RAM available for Docker
- 📄 PostgreSQL dump file (.sql)

## 🚀 Quick Start

### 1. Initial Setup

1. Install Docker Desktop and start it
2. Clone this repository
3. Copy `.env.template` to `.env` and configure your settings:

```bash
cp .env.template .env
```

4. Edit `.env` and set your SQL dump file path:

```properties
# Windows example:
SQL_DUMP_PATH=C:\path\to\your\ad_prod.sql
# Linux/Mac example:
SQL_DUMP_PATH=/path/to/your/ad_prod.sql
```

Be sure to use a `plain` sql-dump of your source database. Only these can be imported to docker container via `psql` in our `build-and-run` scripts.

### 2. Building and Running

#### Windows Users

```powershell
# Double click on the scripts/windows/build-and-run.bat file
# Or run from command prompt:
scripts\windows\build-and-run.bat
```

#### Linux/Mac Users

```bash
# Make the script executable first:
chmod +x scripts/unix/build-and-run.sh
# Then run:
./scripts/unix/build-and-run.sh
```

The application will be available at <http://localhost:3002>

### 3. Database Setup

To export from your production database use `PgAdmin` or a command like this:
```
pg_dump --file "ad_prod.sql" --host "<some>" --port "some" --username "adempiere" --encoding "UTF8" --create --schema "adempiere" --clean "adempiere"
```

But be sure to make a `plain` dump, NOT a binary one.

To import your database dump:

#### Windows Users

```powershell
# Make sure you've configured SQL_DUMP_PATH in .env
# Then double click on scripts/windows/import-db.bat
# Or run from command prompt:
scripts\windows\import-db.bat
```

#### Linux/Mac Users

```bash
# Make sure you've configured SQL_DUMP_PATH in .env
# Make the script executable:
chmod +x scripts/unix/import-db.sh
# Then run:
./scripts/unix/import-db.sh
```

## 🔧 Configuration

### Environment Variables (.env)

- `SQL_DUMP_PATH`: Path to your SQL dump file
- `DB_USER`: Database username (default: adempiere)
- `DB_NAME`: Database name (default: adempiere)

### Ports

- 🌐 Spring Boot application: `3002`
- 🗄️ PostgreSQL: `5432`

### Volumes

- 💾 `postgres_data`: Persistent PostgreSQL data (this is an internal volume within the container and not mapped to a host directory)
- 📝 `logs`: Application logs directory

## 🛠️ Maintenance

### Viewing Logs

Application logs:

```bash
docker-compose logs -f app
```

Database logs:

```bash
docker-compose logs -f db
```

### Database Backup

```bash
docker-compose exec db pg_dump -U adempiere > backup_$(date +%Y%m%d_%H%M%S).sql
```

### Updating the Application

When source code changes are made:

```bash
docker-compose down
docker-compose build --no-cache
docker-compose up -d
```

## ❗ Troubleshooting

### Database Issues

1. **Database fails to initialize:**
- Check container logs: `docker-compose logs db`
- Verify the dump file exists and is accessible
- Check file permissions on your SQL dump

2. **Application can't connect to database:**
- Ensure database container is running: `docker-compose ps`
- Check application logs: `docker-compose logs app`
- Verify database credentials in docker-compose.yml

## 🔒 Security Notes

- Application runs as non-root user inside the container
- All data is persisted in Docker volumes
- For production deployment, use environment variables or Docker secrets for sensitive data

## 💡 Need Help?

Contact the development team or create an issue in the repository for assistance.
