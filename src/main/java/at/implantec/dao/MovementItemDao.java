package at.implantec.dao;

import at.implantec.model.DAOMovementItem;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovementItemDao extends CrudRepository<DAOMovementItem, Integer> {

    List findAllPosByIoid(Long io_id);
}
