package at.implantec.service;

import at.implantec.dao.MovementItemDao;
import at.implantec.model.DAOMovementItem;
import jakarta.annotation.Resource;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class MovementItemServiceImpl implements MovementItemService {
    @Resource
    MovementItemDao movementItemDao;

    @Override
    public List<DAOMovementItem> findAllPosByIoid(Long io_id) {
        return (List<DAOMovementItem>) movementItemDao.findAllPosByIoid(io_id);
    }
}
