#!/bin/bash
# Colors
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

# Function for printing colored status messages
print_status() {
    echo -e "${BLUE}[INFO]${NC} $1"
}
print_success() {
    echo -e "${GREEN}[SUCCESS]${NC} $1"
}
print_error() {
    echo -e "${RED}[ERROR]${NC} $1"
}
print_warning() {
    echo -e "${YELLOW}[WARNING]${NC} $1"
}

# Load environment variables
if [ ! -f .env ]; then
    print_error "No .env file found!"
    print_info "Please copy .env.template to .env and configure your settings"
    exit 1
fi
source .env

if [ -z "$SQL_DUMP_PATH" ]; then
    print_error "SQL_DUMP_PATH is not set in .env file"
    exit 1
fi

if [ ! -f "$SQL_DUMP_PATH" ]; then
    print_error "SQL dump file not found at $SQL_DUMP_PATH"
    print_info "Please check the SQL_DUMP_PATH in your .env file"
    exit 1
fi

# ASCII Art
echo -e "${BLUE}"
cat << "EOF"
 ____  ____    ___                            _   
|  * \| *_ )  |_ *|* **_**  *_*   ___  *_*| |_ 
| | | |  * \   | || '* ` * \| '* \ / * \| '*_| __|
| |_| | |_) |  | || | | | | | |_) | (_) | |  | |_ 
|____/|____/  |___|_| |_| |_| .__/ \___/|_|   \__|
                            |_|                     
EOF
echo -e "${NC}"

print_status "Importing database from $SQL_DUMP_PATH..."

# Get the file extension
FILE_EXT="${SQL_DUMP_PATH##*.}"
FILENAME=$(basename "$SQL_DUMP_PATH")

# Copy the file to the container
if ! docker cp "$SQL_DUMP_PATH" scanner-db:/tmp/"$FILENAME"; then
    print_error "Failed to copy database dump to container"
    exit 1
fi

# Use appropriate command based on file extension
if [[ "$FILE_EXT" == "sql" ]]; then
    print_status "Detected SQL text format, using psql..."
    if docker exec -i scanner-db psql -U "$DB_USER" "$DB_NAME" < "$SQL_DUMP_PATH"; then
        print_success "Database import completed successfully!"
    else
        print_error "Failed to import database with psql"
        exit 1
    fi
elif [[ "$FILE_EXT" == "dmp" || "$FILE_EXT" == "dump" || "$FILE_EXT" == "custom" ]]; then
    print_status "Detected binary format, using pg_restore..."
    if docker exec scanner-db pg_restore -U "$DB_USER" -d "$DB_NAME" --no-owner --disable-triggers --if-exists --clean --exit-on-error /tmp/"$FILENAME"; then
        print_success "Database import completed successfully!"
    else
        print_error "Failed to import database with pg_restore"
        exit 1
    fi
else
    print_warning "Unknown file format: $FILE_EXT"
    print_status "Attempting with psql first..."
    
    if docker exec -i scanner-db psql -U "$DB_USER" "$DB_NAME" < "$SQL_DUMP_PATH"; then
        print_success "Database import completed successfully with psql!"
    else
        print_warning "psql failed, trying pg_restore..."
        if docker exec scanner-db pg_restore -U "$DB_USER" -d "$DB_NAME" --no-owner --disable-triggers --if-exists --clean --exit-on-error /tmp/"$FILENAME"; then
            print_success "Database import completed successfully with pg_restore!"
        else
            print_error "Failed to import database with both psql and pg_restore"
            exit 1
        fi
    fi
fi