package at.implantec.service;

import at.implantec.model.DAOInOutOverview;
import java.util.List;

public interface InOutOverviewService {

    List<DAOInOutOverview> findAllById(Long id);
}
