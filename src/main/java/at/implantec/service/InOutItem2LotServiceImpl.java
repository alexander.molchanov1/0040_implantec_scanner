package at.implantec.service;

import at.implantec.dao.InOutItem2LotDao;
import at.implantec.model.DAOInOutItem2Lot;
import jakarta.annotation.Resource;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class InOutItem2LotServiceImpl implements InOutItem2LotService {

    @Resource
    InOutItem2LotDao inoutItem2LotDao;

    @Override
    public InOutItem2LotDao findInOutItem2LotById(Long id) {
        inoutItem2LotDao.findInOutItem2LotById(id);
        return null;
    }

    @Override
    public void insertInOutItem2Lot(DAOInOutItem2Lot mvl2lot) {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateInOutItem2Lot(DAOInOutItem2Lot iol2lot) {
        inoutItem2LotDao.updateInOutItem2Lot(iol2lot);
    }

    @Override
    public void deleteById(Long id) {
        // TODO Auto-generated method stub

    }

    @Override
    public List<DAOInOutItem2Lot> getAllById(Long id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public DAOInOutItem2Lot getByLineId(Long id) {
        // TODO Auto-generated method stub
        return new DAOInOutItem2Lot();
    }

    @Override
    public String getMessage() {
        return inoutItem2LotDao.getMessage();
    }

    @Override
    public void setMessage(String message) {
        inoutItem2LotDao.setMessage(message);
    }

    @Override
    public HttpStatus getHttpStatus() {
        return inoutItem2LotDao.getHttpStatus();
    }

    @Override
    public void setHttpStatus(HttpStatus httpstatus) {
        inoutItem2LotDao.setHttpStatus(httpstatus);
    }
}
