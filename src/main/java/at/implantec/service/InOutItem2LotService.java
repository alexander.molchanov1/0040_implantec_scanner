package at.implantec.service;

import at.implantec.dao.InOutItem2LotDao;
import at.implantec.model.DAOInOutItem2Lot;
import java.util.List;
import org.springframework.http.HttpStatus;

public interface InOutItem2LotService {

    InOutItem2LotDao findInOutItem2LotById(Long id);

    void insertInOutItem2Lot(DAOInOutItem2Lot mvl2lot);

    void updateInOutItem2Lot(DAOInOutItem2Lot mvl2lot);

    void deleteById(Long id);

    List<DAOInOutItem2Lot> getAllById(Long id);

    DAOInOutItem2Lot getByLineId(Long id);

    String getMessage();

    void setMessage(String message);

    HttpStatus getHttpStatus();

    void setHttpStatus(HttpStatus httpstatus);
}
