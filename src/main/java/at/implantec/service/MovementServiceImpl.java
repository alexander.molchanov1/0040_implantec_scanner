package at.implantec.service;

import at.implantec.dao.MovementDao;
import at.implantec.model.DAOMovement;
import jakarta.annotation.Resource;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class MovementServiceImpl implements MovementService {
    @Resource
    MovementDao movementDao;

    @Override
    public List<DAOMovement> findByIssotrx(String issotrx) {
        return movementDao.findByIssotrx(issotrx);
    }
}
