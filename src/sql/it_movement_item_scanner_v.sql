DROP VIEW it_movement_item_scanner_v;
CREATE
OR REPLACE VIEW it_movement_item_scanner_v AS
WITH
movement AS
(
SELECT 
		it_movement_id, c_bpartner_id, issotrx, documentno 
	FROM 
		it_movement 
	WHERE 
        docstatus = 'DR'
    AND
		it_scanstatus IN ('PR', 'SC')
),
product AS 
(
	SELECT 
		m_product_id, value, name, upc, it_locatorname 
	FROM 
		m_product 
	WHERE 
		iststocked='Y'
	AND 
        isactive='Y'
    AND
        ad_client_id = 1000000
),
bpartner AS
(
	SELECT 
		c_bpartner_id, name
	FROM 
		c_bpartner 
	WHERE 
		isactive='Y'
	AND 
        ad_client_id = 1000000
)
SELECT a.it_movement_id,
       a.documentno,
       a.bpartner_name,
       a.it_movementline_id,
       a.line,
       a.quantity,
       a.it_warehouse_id,
       a.m_product_id,
       a.value,
       a.product_name,
       a.upc,
       a.it_locatorname,
       a.issotrx,
       a.doc_type,
       a.pos_total,
       a.product_total,
       a.qty_scanned,
       a.scanned_total,
       (a.quantity - a.qty_scanned)        AS open_inposition,
       (a.product_total - a.scanned_total) AS open_total
FROM (SELECT mv.it_movement_id,
             mv.documentno,
             cbp.name                                                                                AS bpartner_name,
             mvl.it_movementline_id,
             mvl.line,
             mvl.quantity,
             mvl.it_warehouse_id,
             pr.m_product_id,
             pr.value,
             pr.name                                                                                 AS product_name,
             pr.upc,
             pr.it_locatorname,
             mv.issotrx,
             CASE WHEN mv.issotrx = 'Y' THEN 'MF' ELSE 'MT' END                                      AS doc_type,
             CASE WHEN COALESCE(MAX(mvl2lot.it_lot_id), 0) > 0 THEN SUM(mvl2lot.quantity) ELSE 0 END AS qty_scanned,
             (SELECT COUNT(DISTINCT mvl1.it_movementline_id)
              FROM it_movementline AS mvl1
              WHERE mvl1.it_movement_id = mvl.it_movement_id)                                        AS pos_total,
             (SELECT SUM(mvl1.quantity)
              FROM it_movementline AS mvl1
              WHERE mvl1.it_movement_id = mvl.it_movement_id
              GROUP BY mvl.it_movement_id)                                                           AS product_total,
             COALESCE((SELECT SUM(mvl2lot1.quantity)
                       FROM it_movementline2lot AS mvl2lot1
                       WHERE mvl2lot1.it_movementline_id IN (SELECT mvl1.it_movementline_id
                                                             FROM it_movementline mvl1
                                                             WHERE mvl1.it_movement_id = mvl.it_movement_id)
                         AND mvl2lot1.it_lot_id IS NOT NULL
                       GROUP BY mvl.it_movement_id), 0)                                              AS scanned_total
      FROM movement mv
               JOIN bpartner cbp USING (c_bpartner_id)
               JOIN it_movementline mvl USING (it_movement_id)
               JOIN it_movementline2lot mvl2lot USING (it_movementline_id)
               JOIN product pr ON pr.m_product_id = mvl.m_product_id
      GROUP BY mv.it_movement_id,
               mv.documentno,
               cbp.name,
               mvl.it_movementline_id,
               mvl.line,
               mvl.quantity,
               mvl.it_warehouse_id,
               pr.m_product_id,
               pr.value,
               pr.name,
               pr.upc,
               pr.it_locatorname,
               mv.issotrx
      ORDER BY pr.it_locatorname) a;

ALTER TABLE adempiere.it_movement_item_scanner_v
    OWNER TO adempiere;