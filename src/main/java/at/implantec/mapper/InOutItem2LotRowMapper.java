package at.implantec.mapper;

import at.implantec.model.DAOInOutItem2Lot;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class InOutItem2LotRowMapper implements RowMapper<DAOInOutItem2Lot> {

    @Override
    public DAOInOutItem2Lot mapRow(ResultSet rs, int rowNum) throws SQLException {

        DAOInOutItem2Lot iol2l = new DAOInOutItem2Lot();
        iol2l.setId(rs.getLong("it_inoutline2lot_id"));
        iol2l.setAd_client_id(rs.getInt("ad_client_id"));
        iol2l.setAd_org_id(rs.getInt("ad_org_id"));
        iol2l.setCreated(rs.getTimestamp("created"));
        iol2l.setCreatedby(rs.getInt("createdby"));
        iol2l.setExpected_lot_id(rs.getInt("expected_lot_id"));
        iol2l.setIsactive(rs.getString("isactive"));
        iol2l.setIssotrx(rs.getString("issotrx"));
        iol2l.setIt_locator_id(rs.getInt("it_locator_id"));
        iol2l.setInoutline(rs.getLong("it_inoutline_id"));
        iol2l.setM_product_id(rs.getInt("m_product_id"));
        iol2l.setQtyexpected(rs.getInt("qtyexpected"));
        iol2l.setQuantity(rs.getInt("quantity"));
        iol2l.setT_locator_id(rs.getInt("t_locator_id"));
        iol2l.setUpdated(rs.getTimestamp("updated"));
        iol2l.setUpdatedby(rs.getInt("updatedby"));

        return iol2l;
    }
}
