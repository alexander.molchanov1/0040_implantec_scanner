package at.implantec.service;

import at.implantec.model.DAOMovement;
import java.util.List;

public interface MovementService {

    List<DAOMovement> findByIssotrx(String param);
}
