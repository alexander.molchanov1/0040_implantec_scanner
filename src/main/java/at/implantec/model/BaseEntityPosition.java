package at.implantec.model;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;

/**
 * Base class.
 *
 * @author Alexander Molchanov
 * @version 1.0
 */
@MappedSuperclass
public class BaseEntityPosition {

    @Column(name = "documentno")
    private String io_no;

    @Column(name = "bpartner_name")
    private String bp_name;

    @Column(name = "line")
    private String pos_no;

    @Column(name = "quantity")
    private Integer count_item;

    @Column(name = "it_warehouse_id")
    private String it_warehouse_id;

    @Column(name = "m_product_id")
    private String prod_id;

    @Column(name = "value")
    private String prod_no;

    @Column(name = "product_name")
    private String prod_name;

    @Column(name = "upc")
    private String upc;

    @Column(name = "it_locatorname")
    private String it_locatorname;

    @Column(name = "issotrx")
    private String issotrx;

    @Column(name = "doc_type")
    private String doc_type;

    @Column(name = "qty_scanned")
    private Integer qty_scanned;

    @Column(name = "pos_total")
    private Integer pos_total;

    @Column(name = "product_total")
    private Integer product_total;

    @Column(name = "scanned_total")
    private Integer scanned_total;

    @Column(name = "open_inposition")
    private Integer open_inposition;

    @Column(name = "open_total")
    private Integer open_total;

    public String getIo_no() {
        return io_no;
    }

    public String getBp_name() {
        return bp_name;
    }

    public String getPos_no() {
        return pos_no;
    }

    public Integer getCount_item() {
        return count_item;
    }

    public String getIt_warehouse_id() {
        return it_warehouse_id;
    }

    public String getProd_id() {
        return prod_id;
    }

    public String getProd_no() {
        return prod_no;
    }

    public String getProd_name() {
        return prod_name;
    }

    public String getUpc() {
        return upc;
    }

    public String getIt_locatorname() {
        return it_locatorname;
    }

    public String getIssotrx() {
        return issotrx;
    }

    public String getDoc_type() {
        return doc_type;
    }

    public Integer getQty_scanned() {
        return qty_scanned;
    }

    public Integer getPos_total() {
        return pos_total;
    }

    public Integer getProduct_total() {
        return product_total;
    }

    public Integer getScanned_total() {
        return scanned_total;
    }

    public Integer getOpen_inposition() {
        return open_inposition;
    }

    public Integer getOpen_total() {
        return open_total;
    }
}
