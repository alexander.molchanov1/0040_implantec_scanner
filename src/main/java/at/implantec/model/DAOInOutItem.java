package at.implantec.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import java.util.List;

@Entity
@Table(name = "it_inout_item_scanner_v")
public class DAOInOutItem extends BaseEntityPosition {

    @Id
    @Column(name = "it_inoutline_id")
    private Long line_id;

    @Column(name = "it_inout_id")
    private Long ioid;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "it_inout_lot2item_scanner_v",
            joinColumns = @JoinColumn(name = "it_inoutline_id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "id", insertable = false, updatable = false))
    private List<DAOLot2InOutItem> lots;

    public Long getLine_id() {
        return line_id;
    }

    public Long getIoid() {
        return ioid;
    }
}
