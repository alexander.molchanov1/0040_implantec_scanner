--DROP VIEW it_movement_lot2item_scanner_v;

CREATE
OR REPLACE VIEW it_movement_lot2item_scanner_v AS
WITH
movementline AS
(
SELECT 
		it_movementline_id, m_product_id, it_warehouse_id
	FROM 
		it_movementline
    LEFT JOIN
        it_movement mv USING(it_movement_id) 
	WHERE 
        mv.docstatus = 'DR'
    AND
		mv.it_scanstatus IN ('PR', 'SC')
)
SELECT lot.it_lot_id                     AS id,
       -- ploc2l.it_ploc2lot_id AS id,
       lot.name,
       TO_CHAR(lot.expiry, 'DD.MM.YYYY') AS expiry,
       SUM(COALESCE(ploc2l.quantitylot, 0)) quantitylot,
       mvl.it_movementline_id
FROM movementline mvl
         JOIN m_product p ON p.m_product_id = mvl.m_product_id
         JOIN it_locator loc ON loc.it_warehouse_id = mvl.it_warehouse_id
         JOIN it_lot lot ON lot.m_product_id = p.m_product_id
         JOIN it_ploc ploc ON ploc.m_product_id = p.m_product_id AND loc.it_locator_id = ploc.it_locator_id
         JOIN it_ploc2lot ploc2l ON ploc.it_ploc_id = ploc2l.it_ploc_id AND lot.it_lot_id = ploc2l.it_lot_id
WHERE ploc2l.quantitylot > 0
GROUP BY lot.it_lot_id, lot.name, lot.expiry, mvl.it_movementline_id
ORDER BY mvl.it_movementline_id, lot.expiry ASC;

ALTER TABLE adempiere.it_movement_lot2item_scanner_v
    OWNER TO adempiere;
