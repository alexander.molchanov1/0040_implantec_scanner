package at.implantec.model;

import java.io.Serializable;

public class ResponseScanCode implements Serializable {

    private static final long serialVersionUID = -8091879091924046844L;
    private final String message;

    public ResponseScanCode(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
