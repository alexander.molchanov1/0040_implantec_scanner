package at.implantec.model;

import jakarta.persistence.*;

@Entity
@Table(name = "it_movementline2lot")
public class DAOMovementItem2Lot extends BaseEntityLot2Position {

    @Id
    @Column(name = "it_movementline2lot_id")
    private Long id;

    @Basic
    @Column(name = "it_movementline_id")
    private Long movementline;

    @Transient
    private String code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMovementline() {
        return movementline;
    }

    public void setMovementline(Long movementline) {
        this.movementline = movementline;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
