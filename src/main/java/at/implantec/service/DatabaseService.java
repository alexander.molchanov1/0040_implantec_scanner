package at.implantec.service;

import org.springframework.http.ResponseEntity;

/**
 * Service for database operations
 */
public interface DatabaseService {

    /**
     * Resets the database to its original state by reimporting the SQL dump
     *
     * @return ResponseEntity with the result of the operation
     */
    ResponseEntity<?> resetDatabase();
}
