package at.implantec.service;

import at.implantec.model.DAOInOutItem;
import java.util.List;

public interface InOutItemService {

    List<DAOInOutItem> findAllPosByIoid(Long io_id);
}
