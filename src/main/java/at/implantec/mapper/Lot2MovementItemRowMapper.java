package at.implantec.mapper;

import at.implantec.model.DAOLot2MovementItem;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class Lot2MovementItemRowMapper implements RowMapper<DAOLot2MovementItem> {

    @Override
    public DAOLot2MovementItem mapRow(ResultSet rs, int rowNum) throws SQLException {

        DAOLot2MovementItem lot2mvl = new DAOLot2MovementItem();
        lot2mvl.setId(rs.getLong("id"));
        lot2mvl.setName(rs.getString("name"));
        lot2mvl.setExpiry(rs.getString("expiry"));
        lot2mvl.setQuantitylot(rs.getString("quantitylot"));
        lot2mvl.setMovementline(rs.getLong("it_movementline_id"));

        return lot2mvl;
    }
}
