package at.implantec.controller;

import at.implantec.model.DatabaseResponse;
import at.implantec.service.DatabaseService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/database")
@Tag(name = "Database Operations", description = "Endpoints for database maintenance operations")
public class DatabaseController {

    private final DatabaseService databaseService;

    @Autowired
    public DatabaseController(DatabaseService databaseService) {
        this.databaseService = databaseService;
    }

    @PostMapping("/reset")
    @SecurityRequirement(name = "JWT")
    @Operation(
            summary = "Reset Database",
            description =
                    "Resets the database to its original state by reimporting the SQL dump file specified in application properties")
    @ApiResponses(
            value = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Database reset successful",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = DatabaseResponse.class))),
                @ApiResponse(
                        responseCode = "500",
                        description = "Database reset failed",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = DatabaseResponse.class))),
                @ApiResponse(
                        responseCode = "401",
                        description = "Unauthorized - Authentication required",
                        content = @Content)
            })
    public ResponseEntity<?> resetDatabase() {
        return databaseService.resetDatabase();
    }
}
