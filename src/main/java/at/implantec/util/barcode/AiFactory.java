package at.implantec.util.barcode;

/**
 * @author Alexander Molchanov
 * <p>
 * Version 1.1
 */
public final class AiFactory {

    private AiFactory() {
        super();
    }

    public static AI create(String code, String description, String format) {
        return new AI() {

            @Override
            public String getFormat() {
                return format;
            }

            @Override
            public String getDescription() {
                return description;
            }

            @Override
            public String getCode() {
                return code;
            }
        };
    }
}
