package at.implantec.service;

import at.implantec.dao.InOutOverviewDao;
import at.implantec.model.DAOInOutOverview;
import jakarta.annotation.Resource;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class InOutOverviewServiceImpl implements InOutOverviewService {

    @Resource
    InOutOverviewDao inoutOverviewDao;

    @Override
    public List<DAOInOutOverview> findAllById(Long id) {
        return inoutOverviewDao.findAllById(id);
    }
}
