@echo off
setlocal EnableDelayedExpansion

call :setESC

:: Colors using ANSI escape codes (works in Windows 10+)
set "RED=%ESC%[91m"
set "GREEN=%ESC%[92m"
set "YELLOW=%ESC%[93m"
set "BLUE=%ESC%[94m"
set "NC=%ESC%[0m"

goto main

:setESC
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do rem"') do (
  set ESC=%%b
  exit /B 0
)
exit /B 0

:: Function for status messages
:print_status
echo %BLUE%[INFO]%NC% %~1
exit /b

:print_success
echo %GREEN%[SUCCESS]%NC% %~1
exit /b

:print_error
echo %RED%[ERROR]%NC% %~1
exit /b

:print_warning
echo %YELLOW%[WARNING]%NC% %~1
exit /b

:main

 :: Check for .env file and load it
if not exist .env (
    call :print_error "No .env file found!"
    call :print_status "Please copy .env.template to .env and configure your settings"
    pause
    exit /b 1
)

:: Read .env file
for /f "tokens=1,2 delims==" %%G in (.env) do (
    set "%%G=%%H"
)

:: Validate SQL_DUMP_PATH
if "%SQL_DUMP_PATH%"=="" (
    call :print_error "SQL_DUMP_PATH is not set in .env file"
    pause
    exit /b 1
)

if not exist "%SQL_DUMP_PATH%" (
    call :print_error "SQL dump file not found at %SQL_DUMP_PATH%"
    call :print_status "Please check the SQL_DUMP_PATH in your .env file"
    pause
    exit /b 1
)

cls
echo %BLUE%
echo  ____  ____    ___                            _   
echo ^|  _ \^| __ )  ^|_ _^|_ __ ___  _ __   ___  _ __^| ^|_ 
echo ^| ^| ^| ^|  _ \   ^| ^|^| '_ ` _ \^| '_ \ / _ \^| '__^| __^|
echo ^| ^|_^| ^| ^|_) ^|  ^| ^|^| ^| ^| ^| ^|_) ^| (_) ^| ^|  ^| ^|_ 
echo ^|____/^|____/  ^|___^|_^| ^|_^| ^|_^| .__/ \___/^|_^|   \__^|
echo                             ^|_^|                     
echo.
echo %NC%

call :print_status "Importing database from %SQL_DUMP_PATH%..."
docker exec -i scanner-db psql -U %DB_USER% %DB_NAME% -c "DROP SCHEMA %DB_NAME% CASCADE"
docker exec -i scanner-db psql -U %DB_USER% %DB_NAME% < "%SQL_DUMP_PATH%"

if %ERRORLEVEL% NEQ 0 (
    call :print_error "Failed to import database"
    pause
    exit /b %ERRORLEVEL%
)

call :print_success "Database import completed successfully!"
pause