package at.implantec.dao;

import at.implantec.model.DAOMovement;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovementDao extends CrudRepository<DAOMovement, Integer> {

    List findByIssotrx(String issotrx);
}
