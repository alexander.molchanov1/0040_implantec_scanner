package at.implantec.dao;

import at.implantec.model.DAOInOutItem;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InOutItemDao extends CrudRepository<DAOInOutItem, Integer> {

    List findAllPosByIoid(Long io_id);
}
