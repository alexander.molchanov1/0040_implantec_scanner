package at.implantec.service;

import at.implantec.model.DAOMovementOverview;
import java.util.List;

public interface MovementOverviewService {

    List<DAOMovementOverview> findAllById(Long id);
}
