package at.implantec.service;

import at.implantec.model.DatabaseResponse;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * Implementation of DatabaseService
 */
@Service
public class DatabaseServiceImpl implements DatabaseService {

    private static final Logger logger = LoggerFactory.getLogger(DatabaseServiceImpl.class);

    @Value("${spring.datasource.username}")
    private String dbUser;

    @Value("${spring.datasource.password}")
    private String dbPassword;

    @Value("${spring.datasource.url}")
    private String dbUrl;

    @Value("${sql.dump.path:}")
    private String sqlDumpPath;

    @Override
    public ResponseEntity<?> resetDatabase() {
        logger.info("Attempting database reset");

        // Check if SQL dump path is configured
        if (sqlDumpPath == null || sqlDumpPath.isEmpty()) {
            logger.error("SQL_DUMP_PATH is not configured");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new DatabaseResponse(
                            false, "SQL dump path is not configured. Please check application properties."));
        }

        File sqlDumpFile = new File(sqlDumpPath);
        if (!sqlDumpFile.exists() || !sqlDumpFile.isFile()) {
            logger.error("SQL dump file not found at {}", sqlDumpPath);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new DatabaseResponse(false, "SQL dump file not found at " + sqlDumpPath));
        }

        // Extract database name from database URL
        // Example URL: jdbc:postgresql://db:5432/adempiere
        String dbName = extractDbName(dbUrl);
        if (dbName == null) {
            logger.error("Could not extract database name from URL: {}", dbUrl);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new DatabaseResponse(false, "Could not extract database name from URL."));
        }

        // Get file extension to determine import method
        String fileExt = getFileExtension(sqlDumpFile);

        try {
            boolean success = false;
            String message = "";

            switch (fileExt.toLowerCase()) {
                case "sql":
                    ProcessResult result = importSqlWithPsql(sqlDumpFile, dbName, dbUser);
                    success = result.exitCode == 0;
                    message = success
                            ? "Database reset completed successfully!"
                            : "Failed to reset database. Error: " + result.output;
                    break;
                case "dmp":
                case "dump":
                case "custom":
                    ProcessResult restoreResult = importWithPgRestore(sqlDumpFile, dbName, dbUser);
                    success = restoreResult.exitCode == 0;
                    message = success
                            ? "Database reset completed successfully!"
                            : "Failed to reset database. Error: " + restoreResult.output;
                    break;
                default:
                    // Try both methods
                    ProcessResult psqlResult = importSqlWithPsql(sqlDumpFile, dbName, dbUser);
                    if (psqlResult.exitCode == 0) {
                        success = true;
                        message = "Database reset completed successfully with psql!";
                    } else {
                        ProcessResult pgRestoreResult = importWithPgRestore(sqlDumpFile, dbName, dbUser);
                        success = pgRestoreResult.exitCode == 0;
                        message = success
                                ? "Database reset completed successfully with pg_restore!"
                                : "Failed to reset database with both psql and pg_restore. Error: "
                                        + pgRestoreResult.output;
                    }
                    break;
            }

            logger.info("Database reset result: {}, {}", success, message);
            return ResponseEntity.status(success ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new DatabaseResponse(success, message));

        } catch (Exception e) {
            logger.error("Error during database reset", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new DatabaseResponse(false, "Error during database reset: " + e.getMessage()));
        }
    }

    private String extractDbName(String dbUrl) {
        // Example URL: jdbc:postgresql://db:5432/adempiere
        try {
            String[] parts = dbUrl.split("/");
            return parts[parts.length - 1];
        } catch (Exception e) {
            logger.error("Error extracting database name from URL", e);
            return null;
        }
    }

    private String getFileExtension(File file) {
        String fileName = file.getName();
        int dotIndex = fileName.lastIndexOf('.');
        return (dotIndex == -1) ? "" : fileName.substring(dotIndex + 1);
    }

    private ProcessResult importSqlWithPsql(File sqlDumpFile, String dbName, String dbUser)
            throws IOException, InterruptedException {
        logger.info("Importing SQL dump with psql");

        String command =
                "docker exec -i scanner-db psql -U " + dbUser + " " + dbName + " < " + sqlDumpFile.getAbsolutePath();
        return executeCommand(new String[] {"bash", "-c", command});
    }

    private ProcessResult importWithPgRestore(File sqlDumpFile, String dbName, String dbUser)
            throws IOException, InterruptedException {
        logger.info("Importing SQL dump with pg_restore");

        // First, copy file to container
        String fileName = sqlDumpFile.getName();
        executeCommand(new String[] {"docker", "cp", sqlDumpFile.getAbsolutePath(), "scanner-db:/tmp/" + fileName});

        // Then use pg_restore
        String command = "docker exec scanner-db pg_restore -U " + dbUser + " -d " + dbName
                + " --no-owner --disable-triggers --if-exists --clean --exit-on-error /tmp/" + fileName;

        return executeCommand(new String[] {"bash", "-c", command});
    }

    private ProcessResult executeCommand(String[] command) throws IOException, InterruptedException {
        ProcessBuilder processBuilder = new ProcessBuilder(command);
        processBuilder.redirectErrorStream(true);

        Process process = processBuilder.start();

        StringBuilder output = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line).append("\n");
            }
        }

        int exitCode = process.waitFor();
        return new ProcessResult(exitCode, output.toString());
    }

    private static class ProcessResult {
        final int exitCode;
        final String output;

        ProcessResult(int exitCode, String output) {
            this.exitCode = exitCode;
            this.output = output;
        }
    }
}
