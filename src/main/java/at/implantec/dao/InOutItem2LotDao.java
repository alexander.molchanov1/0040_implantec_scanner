package at.implantec.dao;

import at.implantec.model.DAOInOutItem2Lot;
import java.util.List;
import org.springframework.http.HttpStatus;

public interface InOutItem2LotDao {

    InOutItem2LotDao findInOutItem2LotById(Long id);

    List<DAOInOutItem2Lot> saveInOutItem2LotDuplicate(Long id);

    void insertInOutItem2Lot(DAOInOutItem2Lot iol2lot);

    void updateInOutItem2Lot(DAOInOutItem2Lot iol2lot);

    void deleteById(Long id);

    List<DAOInOutItem2Lot> getAllById(Long id);

    String getMessage();

    void setMessage(String message);

    HttpStatus getHttpStatus();

    void setHttpStatus(HttpStatus httpstatus);
}
