package at.implantec.service;

import at.implantec.model.DAOMovementItem;
import java.util.List;

public interface MovementItemService {

    List<DAOMovementItem> findAllPosByIoid(Long io_id);
}
