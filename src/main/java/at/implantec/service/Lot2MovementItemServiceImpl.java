package at.implantec.service;

import at.implantec.dao.Lot2MovementItemDao;
import at.implantec.model.DAOLot2MovementItem;
import jakarta.annotation.Resource;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class Lot2MovementItemServiceImpl implements Lot2MovementItemService {

    @Resource
    Lot2MovementItemDao lot2MovementItemDao;

    @Override
    public List<DAOLot2MovementItem> findAllPosByMovementline(Long movementline) {
        return (List<DAOLot2MovementItem>) lot2MovementItemDao.findAllPosByMovementline(movementline);
    }
}
