package at.implantec.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "it_product_upc_scanner_v")
public class DAOProductUpc {

    @Id
    @Column(name = "it_line_id")
    private Long id;

    @Column(name = "doctype")
    private String doctype;

    @Column(name = "issotrx")
    private String issotrx;

    @Column(name = "m_product_id")
    private Long product;

    @Column(name = "upc")
    private String upc;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDoctype() {
        return doctype;
    }

    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }

    public String getIssotrx() {
        return issotrx;
    }

    public void setIssotrx(String issotrx) {
        this.issotrx = issotrx;
    }

    public Long getProduct() {
        return product;
    }

    public void setProduct(Long product) {
        this.product = product;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }
}
