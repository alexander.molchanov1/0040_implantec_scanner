--DROP VIEW it_inout_overview_scanner_v;

CREATE
OR REPLACE VIEW it_inout_overview_scanner_v AS
SELECT a.it_inout_id,
       a.it_inoutline_id,
       a.productNumber,
       a.productName,
       a.countShouldBeScanned,
       a.countActuallyScanned,
       a.countShouldBeScanned - a.countActuallyScanned AS counttoscan
FROM (SELECT io.it_inout_id,
             iol.it_inoutline_id,
             pr.value                                                              AS productNumber,
             pr.name                                                               AS productName,
             iol.qtyentered                                                        AS countShouldBeScanned,
             SUM(CASE WHEN iol2l.it_lot_id IS NULL THEN 0 ELSE iol2l.quantity END) AS countActuallyScanned
      FROM it_inout io
               LEFT JOIN it_inoutline iol ON io.it_inout_id = iol.it_inout_id
               LEFT JOIN it_inoutline2lot iol2l ON iol.it_inoutline_id = iol2l.it_inoutline_id
               JOIN m_product pr on pr.m_product_id = iol.m_product_id
      WHERE io.docstatus IN ('DR')
        AND io.it_scanstatus IN ('SC', 'PR')
      GROUP BY io.it_inout_id,
               iol.it_inoutline_id,
               pr.value,
               pr.name,
               iol.qtyentered) a;

ALTER TABLE adempiere.it_inout_overview_scanner_v
    OWNER TO adempiere;