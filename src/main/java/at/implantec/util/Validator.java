package at.implantec.util;

import java.util.ArrayList;
import java.util.Iterator;

public class Validator {

    private final ArrayList<Long> listOfLot = new ArrayList<Long>();
    private final ArrayList<Long> listOfLine = new ArrayList<Long>();

    /**
     * Constructor.
     */
    public Validator() {
        super();
    }

    public boolean isNewLot(long lotid) {

        long lotidold = lotid;

        listOfLot.add(lotid);

        if (listOfLot.size() > 1) {
            lotidold = listOfLot.get(listOfLot.size() - 2);
        }
        return lotidold != lotid;
    }

    public boolean isNewLine(long movementline) {

        long lineidold = movementline;

        listOfLine.add(movementline);

        if (listOfLine.size() > 1) {
            lineidold = listOfLine.get(listOfLine.size() - 2);
        }
        return lineidold != movementline;
    }

    public void clearListOfLot() {
        listOfLot.clear();
    }

    public void clearListOfLine() {
        Iterator<Long> lineIterator = getListOfLine().iterator();
        while (lineIterator.hasNext()) {
            if (lineIterator.next() != null) {
                if (getListOfLine().size() > 1) {
                    lineIterator.remove();
                }
            }
        }
    }

    public int sizeOfListOfLine() {
        return listOfLine.size();
    }

    public int sizeOfListOfLot() {
        return listOfLot.size();
    }

    public Validator getValidator() {
        return this;
    }

    public ArrayList<Long> getListOfLine() {
        return listOfLine;
    }

    public ArrayList<Long> getListOfLot() {
        return listOfLot;
    }
}
