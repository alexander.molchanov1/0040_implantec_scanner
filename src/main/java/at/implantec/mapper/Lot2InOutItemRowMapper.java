package at.implantec.mapper;

import at.implantec.model.DAOLot2InOutItem;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class Lot2InOutItemRowMapper implements RowMapper<DAOLot2InOutItem> {

    @Override
    public DAOLot2InOutItem mapRow(ResultSet rs, int rowNum) throws SQLException {

        DAOLot2InOutItem lot2iol = new DAOLot2InOutItem();
        lot2iol.setId(rs.getLong("id"));
        lot2iol.setName(rs.getString("name"));
        lot2iol.setExpiry(rs.getString("expiry"));
        lot2iol.setQuantitylot(rs.getString("quantitylot"));
        lot2iol.setInoutline(rs.getLong("it_inoutline_id"));

        return lot2iol;
    }
}
