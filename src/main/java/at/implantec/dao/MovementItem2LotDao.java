package at.implantec.dao;

import at.implantec.model.DAOMovementItem2Lot;
import java.util.List;
import org.springframework.http.HttpStatus;

public interface MovementItem2LotDao {

    MovementItem2LotDao findMovementItem2LotById(Long id);

    List<DAOMovementItem2Lot> saveMovementItem2LotDuplicate(Long id);

    void insertMovementItem2Lot(DAOMovementItem2Lot mvl2lot);

    void updateMovementItem2Lot(DAOMovementItem2Lot mvl2lot);

    void deleteById(Long id);

    List<DAOMovementItem2Lot> getAllById(Long id);

    String getMessage();

    void setMessage(String message);

    HttpStatus getHttpStatus();

    void setHttpStatus(HttpStatus httpstatus);
}
