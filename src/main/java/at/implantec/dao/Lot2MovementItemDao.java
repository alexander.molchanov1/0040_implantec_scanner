package at.implantec.dao;

import at.implantec.model.DAOLot2MovementItem;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Lot2MovementItemDao extends CrudRepository<DAOLot2MovementItem, Integer> {

    List findAllPosByMovementline(Long movementline);
}
