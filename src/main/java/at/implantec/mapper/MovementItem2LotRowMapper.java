package at.implantec.mapper;

import at.implantec.model.DAOMovementItem2Lot;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class MovementItem2LotRowMapper implements RowMapper<DAOMovementItem2Lot> {

    @Override
    public DAOMovementItem2Lot mapRow(ResultSet rs, int rowNum) throws SQLException {

        DAOMovementItem2Lot mvi2l = new DAOMovementItem2Lot();
        mvi2l.setId(rs.getLong("it_movementline2lot_id"));
        mvi2l.setAd_client_id(rs.getInt("ad_client_id"));
        mvi2l.setAd_org_id(rs.getInt("ad_org_id"));
        mvi2l.setCreated(rs.getTimestamp("created"));
        mvi2l.setCreatedby(rs.getInt("createdby"));
        mvi2l.setExpected_lot_id(rs.getInt("expected_lot_id"));
        mvi2l.setIsactive(rs.getString("isactive"));
        mvi2l.setIssotrx(rs.getString("issotrx"));
        mvi2l.setIt_locator_id(rs.getInt("it_locator_id"));
        mvi2l.setMovementline(rs.getLong("it_movementline_id"));
        mvi2l.setM_product_id(rs.getInt("m_product_id"));
        mvi2l.setQtyexpected(rs.getInt("qtyexpected"));
        mvi2l.setQuantity(rs.getInt("quantity"));
        mvi2l.setT_locator_id(rs.getInt("t_locator_id"));
        mvi2l.setUpdated(rs.getTimestamp("updated"));
        mvi2l.setUpdatedby(rs.getInt("updatedby"));

        return mvi2l;
    }
}
