package at.implantec.util.barcode;

/**
 * @author Alexander Molchanov
 * <p>
 * Version 1.1
 */
public interface AI {

    String getCode();

    String getDescription();

    String getFormat();
}
