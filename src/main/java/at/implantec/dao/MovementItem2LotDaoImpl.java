package at.implantec.dao;

import at.implantec.mapper.Lot2MovementItemRowMapper;
import at.implantec.mapper.MovementItem2LotRowMapper;
import at.implantec.mapper.ProductUpcRowMapper;
import at.implantec.model.DAOLot2MovementItem;
import at.implantec.model.DAOMovementItem2Lot;
import at.implantec.model.DAOProductUpc;
import at.implantec.util.Validator;
import at.implantec.util.barcode.AI;
import at.implantec.util.barcode.AIs;
import at.implantec.util.barcode.Gs1128Decoder;
import at.implantec.util.barcode.Gs1128Engine;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

/**
 * Central class with the logic of direct writing in line2lot table
 *
 * @author Alexander Molchanov
 * @version 1.8
 */
@Repository
public class MovementItem2LotDaoImpl implements MovementItem2LotDao {

    private final NamedParameterJdbcTemplate template;
    private final Validator validator = new Validator();
    private final Gs1128Engine engine = new Gs1128Engine();
    private final Gs1128Decoder decoder = engine.decoder();
    private List<DAOLot2MovementItem> lots2line;
    private List<DAOProductUpc> produpc;
    private int countlot = 0;
    private String message;
    private HttpStatus httpstatus;

    public MovementItem2LotDaoImpl(NamedParameterJdbcTemplate template) {
        this.template = template;
    }

    @Override
    public MovementItem2LotDao findMovementItem2LotById(Long id) {
        final String sql = "SELECT * FROM it_movementline2lot WHERE it_movementline_id=:id";

        SqlParameterSource param = new MapSqlParameterSource().addValue("it_movementline_id", id);
        return (MovementItem2LotDao) template.query(sql, param, new MovementItem2LotRowMapper());
    }

    @Override
    public List<DAOMovementItem2Lot> saveMovementItem2LotDuplicate(Long movementline) {
        final String sql =
                "SELECT * FROM it_movementline2lot WHERE it_movementline_id=:it_movementline_id ORDER BY it_movementline2lot DESC LIMIT 1";
        SqlParameterSource param = new MapSqlParameterSource().addValue("it_movementline_id", movementline);
        return template.query(sql, param, new MovementItem2LotRowMapper());
    }

    @Override
    public void insertMovementItem2Lot(DAOMovementItem2Lot mvl2lot) {
        final String sql =
                "INSERT INTO it_movementline2lot(ad_client_id, ad_org_id, created, createdby, isactive, issotrx, "
                        + "                                         it_locator_id, it_movementline2lot_id, it_movementline_id, "
                        + "                                         m_product_id, qtyexpected, quantity, t_locator_id, updated, updatedby)"
                        + "                VALUES(1000000, 1000000, now(), 3000135, 'Y', :issotrx, :it_locator_id, nextid(200167, 'N'), :it_movementline_id, :m_product_id, :qtyexpected, :quantity, :t_locator_id, now(), 3000135)";

        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource param = new MapSqlParameterSource()
                .addValue("issotrx", mvl2lot.getIssotrx())
                .addValue("it_locator_id", mvl2lot.getIt_locator_id())
                .addValue("it_movementline_id", mvl2lot.getMovementline())
                .addValue("m_product_id", mvl2lot.getM_product_id())
                .addValue("qtyexpected", 1)
                .addValue("quantity", 1)
                .addValue("t_locator_id", mvl2lot.getT_locator_id());
        template.update(sql, param, holder);
    }

    @Override
    public void updateMovementItem2Lot(DAOMovementItem2Lot mvl2lot) {

        final String sql =
                "UPDATE it_movementline2lot SET it_lot_id=:it_lot_id, quantity=:quantity, updated = now(), updatedby = 3000135 "
                        + "WHERE it_movementline2lot_id=(SELECT it_movementline2lot_id FROM it_movementline2lot WHERE it_movementline_id=:it_movementline_id ORDER BY it_movementline2lot DESC LIMIT 1)";

        long lotid;
        long movementline = mvl2lot.getMovementline();
        if (validator.isNewLine(movementline) || lots2line == null) {
            validator.clearListOfLot();
            countlot = 0;
            lots2line = this.getLotsByLine(movementline);
            produpc = this.getProduct(movementline);
        }
        Map<AI, String> result = decoder.decode(mvl2lot.getCode());
        DAOProductUpc dAOProductUpcRowMapper = produpc.stream()
                .filter(DAOProductUpcRowMapper ->
                        result.get(AIs.GTIN).substring(1).equals(DAOProductUpcRowMapper.getUpc()))
                .findFirst()
                .orElse(null);
        if (dAOProductUpcRowMapper == null) {
            // Response for Scanner
            this.setHttpStatus(HttpStatus.NOT_FOUND);
            this.setMessage("Product cannot be find");
        } else {
            if (result.get(AIs.BATCH_LOT) == null) {
                this.setHttpStatus(HttpStatus.NOT_IMPLEMENTED);
                this.setMessage("LOT does not exist or cannot be scanned");
            } else {
                DAOLot2MovementItem dAOLot2MovementItemRowMapper = lots2line.stream()
                        .filter(DAOLot2MovementItemRowMapper ->
                                result.get(AIs.BATCH_LOT).equals(DAOLot2MovementItemRowMapper.getName()))
                        .findFirst()
                        .orElse(null);
                if (dAOLot2MovementItemRowMapper == null) {
                    // Response for Scanner
                    this.setHttpStatus(HttpStatus.NOT_FOUND);
                    this.setMessage("LOT cannot be find");
                } else {
                    if (this.getCounttoscan(movementline) > 0) {
                        lotid = dAOLot2MovementItemRowMapper.getId();
                        try {
                            DAOMovementItem2Lot mvl2lotcopy = DAOMovementItem2Lot.class.newInstance();
                            mvl2lotcopy = saveMovementItem2LotDuplicate(mvl2lot.getMovementline())
                                    .get(0);
                            if (validator.isNewLot(lotid)) {
                                this.insertMovementItem2Lot(mvl2lotcopy);
                                countlot = 0;
                                countlot = countlot + mvl2lot.getQuantity();

                                KeyHolder holder = new GeneratedKeyHolder();
                                SqlParameterSource param = new MapSqlParameterSource()
                                        .addValue("it_movementline_id", movementline)
                                        .addValue("it_lot_id", lotid)
                                        .addValue("quantity", countlot);
                                template.update(sql, param, holder);

                            } else {
                                countlot = countlot + mvl2lot.getQuantity();
                                KeyHolder holder = new GeneratedKeyHolder();
                                SqlParameterSource param = new MapSqlParameterSource()
                                        .addValue("it_movementline_id", movementline)
                                        .addValue("it_lot_id", lotid)
                                        .addValue("quantity", countlot);
                                template.update(sql, param, holder);
                            }
                            if (this.getCounttoscan(movementline) == 0) {
                                countlot = 0;
                                this.setHttpStatus(HttpStatus.OK);
                                this.setMessage("Position completely scanned");
                                if (this.isComplete(movementline)) {
                                    validator.clearListOfLine();
                                    this.setScannerStatus(movementline, "CO");
                                    this.setHttpStatus(HttpStatus.OK);
                                    this.setMessage("Document completely scanned");
                                }
                            } else {
                                this.setHttpStatus(HttpStatus.OK);
                                this.setMessage("LOT scanned successfully");
                            }
                        } catch (InstantiationException ie) {
                            this.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                            this.setMessage("Error :" + ie);
                        } catch (IllegalAccessException iae) {
                            this.setHttpStatus(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED);
                            this.setMessage("Error: " + iae);
                        }

                    } else {
                        countlot = 0;
                        this.setHttpStatus(HttpStatus.OK);
                        this.setMessage("Position completely scanned");
                    }
                }
            }
        }
    }

    @Override
    public void deleteById(Long id) {
        final String sql = "DELETE FROM it_movementline2lot WHERE it_movementline2lot_id="
                + "(SELECT it_movementline2lot_id FROM it_movementline2lot WHERE it_movementline_id=:it_movementline_id ORDER BY it_movementline2lot DESC LIMIT 1)";

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("it_movementline_id", id);
        template.execute(sql, map, new PreparedStatementCallback<Object>() {
            @Override
            public Object doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                return ps.executeUpdate();
            }
        });
    }

    @Override
    public List<DAOMovementItem2Lot> getAllById(Long id) {
        return template.query(
                "SELECT * FROM it_movementline2lot WHERE it_movementline_id=:id", new MovementItem2LotRowMapper());
    }

    public List<DAOLot2MovementItem> getLotsByLine(Long movementline) {
        SqlParameterSource param = new MapSqlParameterSource().addValue("it_movementline_id", movementline);
        return template.query(
                "SELECT * FROM it_movement_lot2item_scanner_v WHERE it_movementline_id=:it_movementline_id",
                param,
                new Lot2MovementItemRowMapper());
    }

    public List<DAOProductUpc> getProduct(Long movementline) {
        SqlParameterSource param = new MapSqlParameterSource().addValue("line_id", movementline);
        return template.query(
                "SELECT * FROM it_product_upc_scanner_v WHERE line_id=:line_id AND doctype = 'MV'",
                param,
                new ProductUpcRowMapper());
    }

    public Integer getCounttoscan(Long movementline) {
        SqlParameterSource param = new MapSqlParameterSource().addValue("it_movementline_id", movementline);
        return template.queryForObject(
                "SELECT counttoscan FROM it_movement_overview_scanner_v WHERE it_movementline_id =:it_movementline_id",
                param,
                Integer.class);
    }

    public boolean isComplete(Long movementline) {
        SqlParameterSource param = new MapSqlParameterSource().addValue("it_movementline_id", movementline);
        return template.queryForObject(
                        "SELECT open_total FROM it_movement_item_scanner_v WHERE it_movementline_id =:it_movementline_id",
                        param,
                        Integer.class)
                == 0;
    }

    public void setScannerStatus(Long movementline, String scanstatus) {
        final String sql =
                "UPDATE it_movement SET it_scanstatus =:it_scanstatus WHERE it_movement_id = (SELECT it_movement_id FROM it_movementline WHERE it_movementline_id =:it_movementline_id)";
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource param = new MapSqlParameterSource()
                .addValue("it_movementline_id", movementline)
                .addValue("it_scanstatus", scanstatus);
        template.update(sql, param, holder);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getHttpStatus() {
        return httpstatus;
    }

    public void setHttpStatus(HttpStatus httpstatus) {
        this.httpstatus = httpstatus;
    }
}
