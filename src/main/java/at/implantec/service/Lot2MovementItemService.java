package at.implantec.service;

import at.implantec.model.DAOLot2MovementItem;
import java.util.List;

public interface Lot2MovementItemService {

    List<DAOLot2MovementItem> findAllPosByMovementline(Long movementline);
}
