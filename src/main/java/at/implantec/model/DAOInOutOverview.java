package at.implantec.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "it_inout_overview_scanner_v")
public class DAOInOutOverview extends BaseEntityOverview {

    @Id
    @Column(name = "it_inout_id")
    private Long id;

    @Column(name = "it_inoutline_id")
    private Long inoutline;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInoutline() {
        return inoutline;
    }

    public void setInoutline(Long inoutline) {
        this.inoutline = inoutline;
    }
}
