package at.implantec.dao;

import at.implantec.model.DAOMovementOverview;
import java.util.List;

public interface MovementOverviewDao {

    List<DAOMovementOverview> findAllById(Long id);
}
