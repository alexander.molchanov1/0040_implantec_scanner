package at.implantec.model;

import jakarta.persistence.*;

@Entity
@Table(name = "it_inoutline2lot")
public class DAOInOutItem2Lot extends BaseEntityLot2Position {

    @Id
    @Column(name = "it_inoutline2lot_id")
    private Long id;

    @Basic
    @Column(name = "it_inoutline_id")
    private Long inoutline;

    @Transient
    private String code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInoutline() {
        return inoutline;
    }

    public void setInoutline(Long inoutline) {
        this.inoutline = inoutline;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
