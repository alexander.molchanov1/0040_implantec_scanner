﻿-- DROP VIEW it_lot_product_scanner_v;
CREATE
OR REPLACE VIEW it_lot_product_scanner_v AS
WITH 
warehouse AS
(
	SELECT it_warehouse_id, it_site_id  FROM it_warehouse
)
,
product AS 
(
	SELECT 
		M_Product_ID, value 
	FROM 
		M_Product 
	WHERE 
		istStocked='Y'
	AND isactive='Y'
)
,
lots AS
(
SELECT
	ploc.m_product_id,
	p.value,
	ploc2lot.quantitylot AS quantity,
	lot.it_lot_ID::NUMERIC AS it_lot_ID,
	lot.name AS lotname,
	lot.expiry::TIMESTAMP AS expiry,
	l.it_locator_id,
	w.it_warehouse_ID,
	w.it_site_ID
FROM
	product p
	JOIN it_ploc ploc ON p.m_product_id=ploc.m_product_id
	JOIN it_locator l ON ploc.it_locator_id=l.it_locator_id
	JOIN warehouse w ON w.it_warehouse_id=l.it_warehouse_id
	JOIN it_ploc2lot ploc2lot ON ploc2lot.it_ploc_id=ploc.it_ploc_id
	JOIN it_lot lot ON lot.it_lot_id=ploc2lot.it_lot_id
WHERE
	ploc2lot.quantitylot <> 0
)

SELECT m_product_id,
       value,
       quantity,
       it_lot_ID,
       lotname,
       TO_CHAR(expiry, 'DD.MM.YYYY') AS expiry,
       it_locator_id,
       it_warehouse_ID,
       it_site_ID
FROM (SELECT *
      FROM lots) a;

ALTER TABLE adempiere.it_lot_product_scanner_v
    OWNER TO adempiere;