#!/bin/bash

# Colors
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

# Function for printing colored status messages
print_status() {
    echo -e "${BLUE}[INFO]${NC} $1"
}

print_success() {
    echo -e "${GREEN}[SUCCESS]${NC} $1"
}

print_error() {
    echo -e "${RED}[ERROR]${NC} $1"
}

print_warning() {
    echo -e "${YELLOW}[WARNING]${NC} $1"
}

# ASCII Art
echo -e "${BLUE}"
cat << "EOF"
 ____           _              _____                                 
|  _ \         | |            / ____|                                
| |_) |_   _   | |     ___  | |     ___  _ __ ___  _ __   ___  ___ 
|  _ <| | | |  | |    / _ \ | |    / _ \| '_ ` _ \| '_ \ / _ \/ _ \
| |_) | |_| |  | |___| (_) || |___| (_) | | | | | | |_) |  __/  __/
|____/ \__, |  |______\___/  \_____\___/|_| |_| |_| .__/ \___|\___|
        __/ |                                      | |               
       |___/                                       |_|               
EOF
echo -e "${NC}"

print_status "Building Docker images..."
if docker-compose build; then
    print_success "Docker images built successfully"
else
    print_error "Failed to build Docker images"
    exit 1
fi

print_status "Starting services..."
if docker-compose up -d; then
    print_success "Services started successfully!"
    print_success "Application is running at http://localhost:3002"
else
    print_error "Failed to start services"
    exit 1
fi