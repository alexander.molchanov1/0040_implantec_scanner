package at.implantec.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "it_inout_scanner_v")
public class DAOInOut extends BaseEntityHeader {

    @Id
    @Column(name = "it_inout_id")
    private Long id;

    public String getIo_id() {
        return String.valueOf(id);
    }
}
