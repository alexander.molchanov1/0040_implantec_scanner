package at.implantec.dao;

import at.implantec.mapper.MovementOverviewRowMapper;
import at.implantec.model.DAOMovementOverview;
import java.util.List;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class MovementOverviewDaoImpl implements MovementOverviewDao {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private List<DAOMovementOverview> movover;

    public MovementOverviewDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<DAOMovementOverview> findAllById(Long id) {
        final String sql = "SELECT * FROM it_movement_overview_scanner_v WHERE it_movement_id=:it_movement_id";
        SqlParameterSource param = new MapSqlParameterSource().addValue("it_movement_id", id);
        return jdbcTemplate.query(sql, param, new MovementOverviewRowMapper());
    }
}
