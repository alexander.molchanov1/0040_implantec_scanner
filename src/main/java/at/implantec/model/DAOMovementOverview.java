package at.implantec.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "it_movement_overview_scanner_v")
public class DAOMovementOverview extends BaseEntityOverview {

    @Id
    @Column(name = "it_movement_id")
    private Long id;

    @Column(name = "it_movementline_id")
    private Long movementline;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMovementline() {
        return movementline;
    }

    public void setMovementline(Long movementline) {
        this.movementline = movementline;
    }
}
