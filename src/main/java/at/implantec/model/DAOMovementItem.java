package at.implantec.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import java.util.List;

// import at.implantec.model.DAOLot2MovementItem;

@Entity
@Table(name = "it_movement_item_scanner_v")
public class DAOMovementItem extends BaseEntityPosition {

    @Id
    @Column(name = "it_movementline_id")
    private Long line_id;

    @Column(name = "it_movement_id")
    private Long ioid;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "it_movement_lot2item_scanner_v",
            joinColumns = @JoinColumn(name = "it_movementline_id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "id", insertable = false, updatable = false))
    private List<DAOLot2MovementItem> lots;

    public Long getLine_id() {
        return line_id;
    }

    public Long getIoid() {
        return ioid;
    }
}
