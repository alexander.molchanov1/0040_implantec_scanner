--DROP VIEW it_inout_lot2item_scanner_v;

CREATE
OR REPLACE VIEW it_inout_lot2item_scanner_v AS
WITH
inoutline AS
(
SELECT 
		it_inoutline_id, m_product_id, it_warehouse_id
	FROM 
		it_inoutline
    LEFT JOIN
        it_inout io USING(it_inout_id) 
	WHERE 
        io.docstatus = 'DR'
    AND
		io.it_scanstatus IN ('PR', 'SC')
)
SELECT lot.it_lot_id                     AS id,
       -- ploc2l.it_ploc2lot_id AS id,
       lot.name,
       TO_CHAR(lot.expiry, 'DD.MM.YYYY') AS expiry,
       SUM(COALESCE(ploc2l.quantitylot, 0)) quantitylot,
       iol.it_inoutline_id
FROM inoutline iol
         JOIN m_product p ON p.m_product_id = iol.m_product_id
         JOIN it_locator loc ON loc.it_warehouse_id = iol.it_warehouse_id
         JOIN it_lot lot ON lot.m_product_id = p.m_product_id
         JOIN it_ploc ploc ON ploc.m_product_id = p.m_product_id AND loc.it_locator_id = ploc.it_locator_id
         JOIN it_ploc2lot ploc2l ON ploc.it_ploc_id = ploc2l.it_ploc_id AND lot.it_lot_id = ploc2l.it_lot_id
WHERE ploc2l.quantitylot > 0
GROUP BY lot.it_lot_id, lot.name, lot.expiry, iol.it_inoutline_id
ORDER BY iol.it_inoutline_id, lot.expiry ASC;

ALTER TABLE adempiere.it_inout_lot2item_scanner_v
    OWNER TO adempiere;
