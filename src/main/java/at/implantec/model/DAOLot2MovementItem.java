package at.implantec.model;

import jakarta.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "it_movement_lot2item_scanner_v")
public class DAOLot2MovementItem implements Serializable {

    private static final long serialVersionUID = 8495817802073010928L;

    @Id
    @Column(name = "id")
    private Long id;

    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Column(name = "expiry")
    private String expiry;

    @Basic
    @Column(name = "quantitylot")
    private String quantitylot;

    @Basic
    @Column(name = "it_movementline_id", nullable = false, insertable = false, updatable = false)
    private Long movementline;

    /*	@JsonBackReference
    	@ManyToOne(optional = false)
    	@JoinColumn(name = "it_movementline_id") // name of the foreign key column
    	private DAOMovementItem dAOMovementItem;
    */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getQuantitylot() {
        return quantitylot;
    }

    public void setQuantitylot(String quantitylot) {
        this.quantitylot = quantitylot;
    }

    public Long getMovementline() {
        return movementline;
    }

    public void setMovementline(Long movementline) {
        this.movementline = movementline;
    }

    /*
    	public DAOMovementItem getDAOMovementItem() {
    		return dAOMovementItem;
    	}
    */
}
