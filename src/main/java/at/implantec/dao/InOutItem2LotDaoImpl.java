package at.implantec.dao;

import at.implantec.mapper.InOutItem2LotRowMapper;
import at.implantec.mapper.Lot2InOutItemRowMapper;
import at.implantec.mapper.ProductUpcRowMapper;
import at.implantec.model.DAOInOutItem2Lot;
import at.implantec.model.DAOLot2InOutItem;
import at.implantec.model.DAOProductUpc;
import at.implantec.util.Validator;
import at.implantec.util.barcode.AI;
import at.implantec.util.barcode.AIs;
import at.implantec.util.barcode.Gs1128Decoder;
import at.implantec.util.barcode.Gs1128Engine;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

/**
 * Central class with the logic of direct writing in line2lot table
 *
 * @author Alexander Molchanov
 * @version 1.8
 */
@Repository
public class InOutItem2LotDaoImpl implements InOutItem2LotDao {

    private final NamedParameterJdbcTemplate template;
    private final Validator validator = new Validator();
    private final Gs1128Engine engine = new Gs1128Engine();
    private final Gs1128Decoder decoder = engine.decoder();
    private List<DAOLot2InOutItem> lots2line;
    private List<DAOProductUpc> produpc;
    private int countlot = 0;
    private String message;
    private HttpStatus httpstatus;

    public InOutItem2LotDaoImpl(NamedParameterJdbcTemplate template) {
        this.template = template;
    }

    @Override
    public InOutItem2LotDao findInOutItem2LotById(Long id) {
        final String sql = "SELECT * FROM it_inoutline2lot WHERE it_inoutline_id=:id";

        SqlParameterSource param = new MapSqlParameterSource().addValue("it_inoutline_id", id);
        return (InOutItem2LotDao) template.query(sql, param, new InOutItem2LotRowMapper());
    }

    @Override
    public List<DAOInOutItem2Lot> saveInOutItem2LotDuplicate(Long inoutline) {
        final String sql =
                "SELECT * FROM it_inoutline2lot WHERE it_inoutline_id=:it_inoutline_id ORDER BY it_inoutline2lot DESC LIMIT 1";
        SqlParameterSource param = new MapSqlParameterSource().addValue("it_inoutline_id", inoutline);
        return template.query(sql, param, new InOutItem2LotRowMapper());
    }

    @Override
    public void insertInOutItem2Lot(DAOInOutItem2Lot iol2lot) {
        final String sql =
                "INSERT INTO it_inoutline2lot(ad_client_id, ad_org_id, created, createdby, isactive, issotrx, "
                        + "                                         it_locator_id, it_inoutline2lot_id, it_inoutline_id, "
                        + "                                         m_product_id, qtyexpected, quantity, t_locator_id, updated, updatedby)"
                        + "                VALUES(1000000, 1000000, now(), 3000135, 'Y', :issotrx, :it_locator_id, nextid(200167, 'N'), :it_inoutline_id, :m_product_id, :qtyexpected, :quantity, :t_locator_id, now(), 3000135)";

        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource param = new MapSqlParameterSource()
                .addValue("issotrx", iol2lot.getIssotrx())
                .addValue("it_locator_id", iol2lot.getIt_locator_id())
                .addValue("it_inoutline_id", iol2lot.getInoutline())
                .addValue("m_product_id", iol2lot.getM_product_id())
                .addValue("qtyexpected", 1)
                .addValue("quantity", 1)
                .addValue("t_locator_id", iol2lot.getT_locator_id());
        template.update(sql, param, holder);
    }

    @Override
    public void updateInOutItem2Lot(DAOInOutItem2Lot iol2lot) {

        final String sql =
                "UPDATE it_inoutline2lot SET it_lot_id=:it_lot_id, quantity=:quantity, updated = now(), updatedby = 3000135 "
                        + "WHERE it_inoutline2lot_id=(SELECT it_inoutline2lot_id FROM it_inoutline2lot WHERE it_inoutline_id=:it_inoutline_id ORDER BY it_inoutline2lot DESC LIMIT 1)";

        long lotid;
        long inoutline = iol2lot.getInoutline();
        if (validator.isNewLine(inoutline) || lots2line == null) {
            validator.clearListOfLot();
            countlot = 0;
            lots2line = this.getLotsByLine(inoutline);
            produpc = this.getProduct(inoutline);
        }
        Map<AI, String> result = decoder.decode(iol2lot.getCode());
        DAOProductUpc dAOProductUpcRowMapper = produpc.stream()
                .filter(DAOProductUpcRowMapper ->
                        result.get(AIs.GTIN).substring(1).equals(DAOProductUpcRowMapper.getUpc()))
                .findFirst()
                .orElse(null);
        if (dAOProductUpcRowMapper == null) {
            // Response for Scanner
            this.setHttpStatus(HttpStatus.NOT_FOUND);
            this.setMessage("Product cannot be find");
        } else {
            if (result.get(AIs.BATCH_LOT) == null) {
                this.setHttpStatus(HttpStatus.NOT_IMPLEMENTED);
                this.setMessage("LOT does not exist or cannot be scanned");
            } else {
                DAOLot2InOutItem dAOLot2InOutItemRowMapper = lots2line.stream()
                        .filter(DAOLot2InOutItemRowMapper ->
                                result.get(AIs.BATCH_LOT).equals(DAOLot2InOutItemRowMapper.getName()))
                        .findFirst()
                        .orElse(null);
                if (dAOLot2InOutItemRowMapper == null
                        && dAOProductUpcRowMapper.getIssotrx().equals("Y")) {
                    // Response for Scanner
                    this.setHttpStatus(HttpStatus.NOT_FOUND);
                    this.setMessage("LOT cannot be find");
                    System.out.println("IsSoTrx: " + dAOProductUpcRowMapper.getIssotrx());
                } else {
                    if (this.getCounttoscan(inoutline) > 0) {
                        if (dAOProductUpcRowMapper.getIssotrx().equals("N")) {
                            lotid = this.findORinsertLotByName(
                                    result.get(AIs.BATCH_LOT),
                                    result.get(AIs.EXPIRY),
                                    dAOProductUpcRowMapper.getProduct());
                        } else {
                            lotid = dAOLot2InOutItemRowMapper.getId();
                        }
                        try {
                            DAOInOutItem2Lot iol2lotcopy = DAOInOutItem2Lot.class.newInstance();
                            iol2lotcopy = saveInOutItem2LotDuplicate(iol2lot.getInoutline())
                                    .get(0);
                            if (validator.isNewLot(lotid)) {
                                this.insertInOutItem2Lot(iol2lotcopy);
                                countlot = 0;
                                countlot = countlot + iol2lot.getQuantity();

                                KeyHolder holder = new GeneratedKeyHolder();
                                SqlParameterSource param = new MapSqlParameterSource()
                                        .addValue("it_inoutline_id", inoutline)
                                        .addValue("it_lot_id", lotid)
                                        .addValue("quantity", countlot);
                                template.update(sql, param, holder);

                            } else {
                                countlot = countlot + iol2lot.getQuantity();
                                KeyHolder holder = new GeneratedKeyHolder();
                                SqlParameterSource param = new MapSqlParameterSource()
                                        .addValue("it_inoutline_id", inoutline)
                                        .addValue("it_lot_id", lotid)
                                        .addValue("quantity", countlot);
                                template.update(sql, param, holder);
                            }
                            if (this.getCounttoscan(inoutline) == 0) {
                                countlot = 0;
                                this.setHttpStatus(HttpStatus.OK);
                                this.setMessage("Position completely scanned");
                                if (this.isComplete(inoutline)) {
                                    validator.clearListOfLine();
                                    this.setScannerStatus(inoutline, "CO");
                                    this.setHttpStatus(HttpStatus.OK);
                                    this.setMessage("Document completely scanned");
                                }
                            } else {
                                this.setHttpStatus(HttpStatus.OK);
                                this.setMessage("LOT scanned successfully");
                            }
                        } catch (InstantiationException ie) {
                            this.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                            this.setMessage("Error :" + ie);
                        } catch (IllegalAccessException iae) {
                            this.setHttpStatus(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED);
                            this.setMessage("Error: " + iae);
                        }

                    } else {
                        countlot = 0;
                        this.setHttpStatus(HttpStatus.OK);
                        this.setMessage("Position completely scanned");
                    }
                }
            }
        }
    }

    @Override
    public void deleteById(Long id) {
        final String sql = "DELETE FROM it_inoutline2lot WHERE it_inoutline2lot_id="
                + "(SELECT it_inoutline2lot_id FROM it_inoutline2lot WHERE it_inoutline_id=:it_inoutline_id ORDER BY it_inoutline2lot DESC LIMIT 1)";

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("it_inoutline_id", id);
        template.execute(sql, map, new PreparedStatementCallback<Object>() {
            @Override
            public Object doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                return ps.executeUpdate();
            }
        });
    }

    @Override
    public List<DAOInOutItem2Lot> getAllById(Long id) {
        return template.query("SELECT * FROM it_inoutline2lot WHERE it_inoutline_id=:id", new InOutItem2LotRowMapper());
    }

    public List<DAOLot2InOutItem> getLotsByLine(Long inoutline) {
        SqlParameterSource param = new MapSqlParameterSource().addValue("it_inoutline_id", inoutline);
        return template.query(
                "SELECT * FROM it_inout_lot2item_scanner_v WHERE it_inoutline_id=:it_inoutline_id",
                param,
                new Lot2InOutItemRowMapper());
    }

    public List<DAOProductUpc> getProduct(Long inoutline) {
        SqlParameterSource param = new MapSqlParameterSource().addValue("line_id", inoutline);
        return template.query(
                "SELECT * FROM it_product_upc_scanner_v WHERE line_id=:line_id AND doctype = 'IO'",
                param,
                new ProductUpcRowMapper());
    }

    public Integer getCounttoscan(Long inoutline) {
        SqlParameterSource param = new MapSqlParameterSource().addValue("it_inoutline_id", inoutline);
        return template.queryForObject(
                "SELECT counttoscan FROM it_inout_overview_scanner_v WHERE it_inoutline_id =:it_inoutline_id",
                param,
                Integer.class);
    }

    public boolean isComplete(Long inoutline) {
        SqlParameterSource param = new MapSqlParameterSource().addValue("it_inoutline_id", inoutline);
        return template.queryForObject(
                        "SELECT open_total FROM it_inout_item_scanner_v WHERE it_inoutline_id =:it_inoutline_id",
                        param,
                        Integer.class)
                == 0;
    }

    public void setScannerStatus(Long inoutline, String scanstatus) {
        final String sql = "UPDATE it_inout SET it_scanstatus =:it_scanstatus WHERE it_inout_id = "
                + "(SELECT it_inout_id FROM it_inoutline WHERE it_inoutline_id =:it_inoutline_id)";
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource param = new MapSqlParameterSource()
                .addValue("it_inoutline_id", inoutline)
                .addValue("it_scanstatus", scanstatus);
        template.update(sql, param, holder);
    }

    public String getIssotrx(Long inoutline) {
        SqlParameterSource param = new MapSqlParameterSource().addValue("it_inoutline_id", inoutline);
        return template.queryForObject(
                "SELECT issotrx FROM it_inout WHERE it_inout_id = "
                        + "(SELECT it_inout_id FROM it_inoutlineWHERE it_inoutline_id =:it_inoutline_id)",
                param,
                String.class);
    }

    public Long findORinsertLotByName(String name, String expiry, Long product_id) {
        final String sql =
                "WITH s AS (" + "    SELECT ad_client_id, ad_org_id, created, createdby, expiry, isactive, isdefault,"
                        + "           it_lot_id, name, current_valuation, m_product_id, updated, updatedby "
                        + "    FROM it_lot AS l_row "
                        + "    WHERE l_row.name =:name "
                        + "    AND l_row.expiry =:expiry::TIMESTAMP "
                        + "    AND l_row.m_product_id =:m_product_id"
                        + "), i AS ("
                        + "    INSERT INTO it_lot ("
                        + "            ad_client_id, ad_org_id, created, createdby, expiry, isactive, isdefault,"
                        + "			it_lot_id, name, current_valuation, m_product_id, updated, updatedby)"
                        + "	SELECT 1000000 AS ad_client_id, "
                        + "           1000000 AS ad_org_id, "
                        + "           now() AS created, "
                        + "           3000135 AS createdby, "
                        + "           :expiry::TIMESTAMP AS expiry, "
                        + "           'Y' AS isactive, "
                        + "           'N' AS isdefault, "
                        + "           nextid(200163, 'N') AS it_lot_id, "
                        + "           :name AS name, "
                        + "           1 AS current_valuation,"
                        + "           :m_product_id AS m_product_id, "
                        + "           now() AS updated, "
                        + "           3000135 AS updatedby "
                        + "	WHERE NOT EXISTS (SELECT 1 FROM s) "
                        + "    RETURNING it_lot_id "
                        + ") "
                        + "SELECT it_lot_id FROM i "
                        + "UNION ALL "
                        + "SELECT it_lot_id FROM s";
        SqlParameterSource param = new MapSqlParameterSource()
                .addValue("m_product_id", product_id)
                .addValue("name", name)
                .addValue("expiry", expiry);
        return template.queryForObject(sql, param, Long.class);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getHttpStatus() {
        return httpstatus;
    }

    public void setHttpStatus(HttpStatus httpstatus) {
        this.httpstatus = httpstatus;
    }
}
