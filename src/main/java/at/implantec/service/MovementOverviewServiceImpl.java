package at.implantec.service;

import at.implantec.dao.MovementOverviewDao;
import at.implantec.model.DAOMovementOverview;
import jakarta.annotation.Resource;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class MovementOverviewServiceImpl implements MovementOverviewService {

    @Resource
    MovementOverviewDao movementOverviewDao;

    @Override
    public List<DAOMovementOverview> findAllById(Long id) {
        return movementOverviewDao.findAllById(id);
    }
}
