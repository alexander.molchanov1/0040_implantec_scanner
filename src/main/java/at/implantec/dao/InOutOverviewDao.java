package at.implantec.dao;

import at.implantec.model.DAOInOutOverview;
import java.util.List;

public interface InOutOverviewDao {

    List<DAOInOutOverview> findAllById(Long id);
}
